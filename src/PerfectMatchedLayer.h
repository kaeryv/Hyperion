#pragma once

#include "utils/CKinds.h"

#include "Field.h"
#include "Log.h"
#include <vector>
#include <math.h>
#include <map>
#include <sstream>
#include <fstream>

class PerfectMatchedLayer
{
  public:
	PerfectMatchedLayer();
	void generate(std::size_t pml, std::size_t length, const Field<real> &epsilon);
	std::size_t getWidth() { return m_Width; }
	void loadFromFile(std::string filename);
	~PerfectMatchedLayer();

  protected:
	Field<real> bE, cE, bH, cH;

	real *sigmaE, *alphaE, *kappaE, *sigmaH, *alphaH, *kappaH;

	std::size_t m_Width;

  private:
	real Imp0;
	real epseff;
	real lnRtheo;
	real msk;
	real rapSmSo;
	real alphamax;
	real kappamax;
	real ma;
	std::map<std::string, real> m_Parameters;
};
