#pragma once

#include <iostream>
#include <tuple>

template <typename T>
struct Vec2 {
	T x, y;
	Vec2(): x(0), y(0){

    }
    Vec2(const T i, const T i1){
		x = i;
		y = i1;
	}
	Vec2 operator+(const Vec2& arg) {
		return std::tie(x, y) + std::tie(arg.x, arg.y);
	}
	Vec2 operator*(const Vec2& arg) {
		return { x * arg.x, y * arg.y };
	}
	Vec2 operator/(const Vec2& arg) {
		return { x / arg.x, y / arg.y };
	}
	Vec2 operator-(const Vec2& arg) {
		return { x - arg.x, y - arg.y };
	}
	Vec2 operator*(const T& alpha) {
		return { alpha*x, alpha*y };
	}
	Vec2 operator+(const T& alpha)  {
		return { x + alpha, y + alpha };
	}

	friend std::istream& operator >> (std::istream& is, Vec2& elem)
	{
		is >> elem.x >> elem.y;
		return is;
	}
	friend std::ostream& operator << (std::ostream& os, const Vec2& elem)
	{
		os << "Vec2 {" << elem.x << ","<< elem.y << "}";
		return os;
	}
	
};

template <typename T>
struct Vec3 {
    Vec3(T i, T i1, T i2):x(i),y(i1),z(i2){

    }
    Vec3():x(0), y(0), z(0){

    }

    T x, y, z;
	void operator=(const T& arg){
		x = arg; 
		y = arg; 
		z = arg;
	}
	void operator=(const Vec3& arg) {
		//(*this) = std::tie(arg.x, arg.y, arg.z);
		x = arg.x;
		y = arg.y;
		z = arg.z;
	}
	bool operator<(const Vec3& arg){
		return std::tie(x, y, z) < std::tie(arg.x, arg.y, arg.z);
	}
	bool operator>(const Vec3& arg){
		return std::tie(x, y, z) > std::tie(arg.x, arg.y, arg.z);
	}
	Vec3 operator+(const Vec3& arg) {
		return Vec3{ x + arg.x, y + arg.y, z + arg.z };
	}
	Vec3<T> operator+(const T& arg) {
		return Vec3<T> {x + arg, y + arg, z + arg};
	}
	Vec3 operator*(const Vec3& arg) {
		return std::tie(x, y, z) * std::tie(arg.x, arg.y, arg.z);
	}
	Vec3 operator/(const Vec3& arg) {
		return std::tie(x, y, z) / std::tie(arg.x, arg.y, arg.z);
	}
	Vec3 operator-(const Vec3& arg) {
		return std::tie(x, y, z) - std::tie(arg.x, arg.y, arg.z);
	}
	void operator+=(const Vec3& arg) {
		//(*this) = std::tie(x, y, z) + std::tie(arg.x, arg.y, arg.z);
		x += arg.x;
		y += arg.y;
		z += arg.z;
	}
	friend std::istream& operator >> (std::istream& is, Vec3& elem)
	{
		is >> elem.x >> elem.y >> elem.z;
		return is;
	}
	friend std::ostream& operator << (std::ostream& os, const Vec3& elem)
	{
		os << "Vec3 { " << elem.x << ", "<< elem.y << ", " << elem.z << "}";
		return os;
	}
	
};
