#pragma once

#include "CKinds.h"

namespace Constants {
	constexpr real pi = 3.141592653589793;
	constexpr real twopi = pi*2.0;
	constexpr real mu0 = 4.0*pi *1e-7;
	constexpr real c = 299792458.0;
    constexpr complex Icomp = complex(0.0, 1.0);
}