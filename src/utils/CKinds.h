#pragma once

#include <complex>

#define CMAXWELL_KIND_DOUBLE

#ifdef CMAXWELL_KIND_DOUBLE
	typedef double real;
#else
	typedef float real;
#endif

typedef std::complex<real> complex;
using String = std::string;