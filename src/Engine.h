#pragma once

/*
    Internal includes
*/
#include "YeeDomain.h"
#include "Geometry.h"
#include "Log.h"
#include "utils/Constants.h"
#include "GeometryBaker.h"
#include "ConvolutionPmlBoundary.h"
#include "AnalysisManager.h"
#include "AuxilaryYeeDomain.h"
#include "HuygensInterface.h"
#include "Sphere.h"

#include <fstream>
#include <sstream>
#include <array>

#include "utils/json.hpp"

using json = nlohmann::json;

namespace Hyperion
{
enum class EngineError
{
	NONE,
	WRONG_SIZE,
	BAD_ALLOC
};
enum class IOError
{
	NONE,
	FILE_NOT_FOUND,
	SYNTAX_ERROR,
	TYPE_ERROR
};

class Engine : public YeeDomain
{
  public:
	Engine(Vec3<std::size_t> size, const std::string &name);

	void init();

	void update(std::size_t T);

	void setWatchdogCondition(int cond) { m_WatchdogCond = cond; }

	GeometryBaker &getGeometryManager() { return gb; }

	AnalysisManager &getAnalysManager() { return am; }

	ConvolutionPmlBoundary &getPML() { return pml; }

	AuxilaryYeeDomain &getAuxilaryDomain() { return aux1D; }
	const AuxilaryYeeDomain &getAuxilaryDomain() const { return aux1D; }

	HuygensInterface &getHuygensInterface() { return hi; }
	const HuygensInterface &getHuygensInterface() const { return hi; }

	IOError finalize();
	~Engine();

  public:
	bool request_close = false;
	int last_frametime = 0.f;

  protected:
	GeometryBaker gb;
	HuygensInterface hi;
	ConvolutionPmlBoundary pml;
	AnalysisManager am;
	AuxilaryYeeDomain aux1D;

  private:
	String m_Name;
	int m_WatchdogCond = 1000;
};
}