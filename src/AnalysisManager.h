#pragma once

#include "VectorField.h"
#include "utils/Constants.h"
#include "Field.h"
#include "Log.h"
#include "AuxilaryYeeDomain.h"

#include <vector>
#include <iterator>
#include <fstream>
#include <map>



/*
 * Structure for storing a Flow measurement plane.
 */



struct FlowPlaneProbe {
    std::size_t offset;
    Vec2<std::size_t> position;
    Vec2<std::size_t> size;
};

enum class SnapshotAxis {
	X, Y, Z
};
enum class SnapshotFieldType {
	E, H, E1D, H1D
};
template <typename T>
struct SnapshotField : Field<T>
{

	SnapshotAxis axis = SnapshotAxis::Z;
	SnapshotFieldType type = SnapshotFieldType::E;
	std::vector<real> frequencies;
	std::size_t level;
	Field<real>* p_probed_field;
};

class AnalysisManager
{
	
public:
    AnalysisManager();
    ~AnalysisManager();
	std::size_t getHorizontalPlaneSurface();
    real updateWatchdog(std::size_t T, const Field<real>& field);
    void updateSource(const AuxilaryYeeDomain&, const std::size_t T);
    void init(const real fMin, const real fMax, const std::size_t n, const FlowPlaneProbe& h_flow_plane, const FlowPlaneProbe& l_flow_plane);
    void update(const VectorField& E, const VectorField& H, const std::size_t T);
    void poyntingVector(std::string sim_id);
	void updatePoyntingSnapshot(const std::size_t T);
	void registerSnapshot(SnapshotAxis axis, SnapshotFieldType type, std::size_t level, std::vector<real> frequency, Field<real>* field_ref);
private:
    std::vector<real> m_Frequencies;
    std::vector<real> m_Watchdog;
    std::vector<complex> E2DyplanR_1D, H2DzplanR_1D, H2DyplanR_1D, E2DzplanR_1D, SourceEz, SourceHy;
    real indice_IN = 1.0;
    std::size_t n_Freq;   
    Vec2<std::size_t> horiz_begin, horiz_size, lateral_begin, lateral_size;
    std::size_t flow_R, flow_L;

    Field<complex> E2DyplanR, E2DzplanR, H2DyplanR, H2DzplanR;
    Field<complex> E2DyplanT, E2DzplanT, H2DyplanT, H2DzplanT;
    Field<complex> E2DxplanLR, E2DzplanLR, H2DxplanLR, H2DzplanLR;
	Field<complex> E2DxplanLL, E2DzplanLL, H2DxplanLL, H2DzplanLL;

    std::vector<real> m_Iref, m_Itran, m_ILat, m_ISrc;

	std::vector<SnapshotField<complex>> m_poynting_snapshots;
};

