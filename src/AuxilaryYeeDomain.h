#pragma once

#include "utils/CKinds.h"
#include "Log.h"
#include "utils/Constants.h"
#include "Field.h"

#include <vector>

struct TimeDomainSourceParams
{
    real frequency = 0.05;
    real delta = 80;
    real power = 2.0;
    std::size_t position = 10;
    enum Profile
    {
        Gaussian,
        Mexihat
    } type;
};

class AuxilaryYeeDomain
{
  public:
    AuxilaryYeeDomain();
    void init(TimeDomainSourceParams &sp, std::size_t length, std::vector<real> *eps);
    const std::vector<real> &getEz() const { return Ez_1D; }
    const std::vector<real> &getHy() const { return Hy_1D; }
    const std::vector<real> &getHx() const { return Hx_1D; }
    const Vec2<real> getSource(const std::size_t t) const;
    void update(std::size_t T, const Field<real> &eps);
    ~AuxilaryYeeDomain() {}

  private:
    // Fields
    Field<real> Ez_1D, Hy_1D, Hx_1D;
    // ABC
    real E1, E2, E3, H1, H2, H3;
    // Courant condition
    const real fac_cfl = 0.5;
    // Pointer to dielectric
    std::vector<real> *m_Epsilon;
    TimeDomainSourceParams m_Source;
    std::size_t m_Length;
};
