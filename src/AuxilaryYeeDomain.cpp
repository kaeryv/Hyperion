#include "./AuxilaryYeeDomain.h"

AuxilaryYeeDomain::AuxilaryYeeDomain()
{
    H1 = 0;
    H2 = 0;
    H3 = 0;
    E1 = 0;
    E2 = 0;
    E3 = 0;
}

void AuxilaryYeeDomain::init(TimeDomainSourceParams &sp, std::size_t length, std::vector<real> *eps)
{
    Log::Hbar();
    Log::Info("Created auxilary 1D FDTD source-grid.");

    m_Length = length;
    m_Source = sp;

    Ez_1D.init(m_Length, 1, 1, "Ez1D");
    Hy_1D.init(m_Length, 1, 1, "Hy1D");

    Ez_1D.zeros();
    Hy_1D.zeros();

    m_Epsilon = eps;
}
const Vec2<real> AuxilaryYeeDomain::getSource(const std::size_t T) const
{
    Vec2<real> source;
    // Now clearly separated time definition from source calculus to avoid those errors
    real t1 = (real)T + 1.5 - m_Source.delta; // Correction : FORGOT THE 0.5 timestep AGAIN
    real t2 = T - m_Source.delta;

    if (m_Source.type == TimeDomainSourceParams::Profile::Mexihat)
    {
        source.x = -(static_cast<real>(1.) - static_cast<real>(2.) * pow(Constants::pi * m_Source.frequency * (t1), 2)) * exp(-pow(Constants::pi * m_Source.frequency * (t1), 2));
        source.y = (static_cast<real>(1.) - static_cast<real>(2.) * pow(Constants::pi * m_Source.frequency * (t2), 2)) * exp(-pow(Constants::pi * m_Source.frequency * (t2), 2));
    }
    else if (m_Source.type == TimeDomainSourceParams::Profile::Gaussian)
    {
        source.x = -exp(-pow((t1 - 0.5) * m_Source.frequency, m_Source.power));
        source.y = exp(-pow(t2 * m_Source.frequency, m_Source.power));
    }
    return source;
}
void AuxilaryYeeDomain::update(std::size_t T, const Field<real> &eps)
{
    std::size_t SOURCE_X = m_Source.position;
    Vec2<real> source = getSource(T);
    for (std::size_t x = 0; x < m_Length - 1; x++)
    {
        Hy_1D[x] += fac_cfl * (Ez_1D[x + 1] - Ez_1D[x]);
    }
    Hy_1D[m_Length - 1] += fac_cfl * (E3 - Ez_1D[m_Length - 1]);

    Hy_1D[SOURCE_X - 1] += source.y;
    H3 = H2;
    H2 = H1;
    H1 = Hy_1D[0];

    Ez_1D[0] += fac_cfl * (Hy_1D[0] - H3) / (*m_Epsilon)[0];

    for (std::size_t x = 1; x <= m_Length - 1; x++)
    {
        Ez_1D[x] += fac_cfl * (Hy_1D[x] - Hy_1D[x - 1]) / (*m_Epsilon)[x];
    }

    Ez_1D[SOURCE_X] += source.x;
    E3 = E2;
    E2 = E1;
    E1 = Ez_1D[m_Length - 1];
}