#include "./ConvolutionPmlBoundary.h"



ConvolutionPmlBoundary::ConvolutionPmlBoundary()
{
    m_IsInitialized = false;
}

void ConvolutionPmlBoundary::init(const Vec3<std::size_t>& size, std::size_t width, const Field<real>& epsilon)
{
    m_Width = width;
    n = size;

    Log::Info("Initializing auxilary CPML psi tensors.");
    {
        psiExy1.init(n.x, m_Width, n.z, "psiExy1");
        psiExy2.init(n.x, m_Width, n.z, "psiExy2");
        psiExz1.init(n.x, n.y, m_Width, "psiExz1");
        psiExz2.init(n.x, n.y, m_Width, "psiExz2");

        psiEyx1.init(m_Width, n.y, n.z, "psiEyx1");
        psiEyx2.init(m_Width, n.y, n.z, "psiEyx2");
        psiEyz1.init(n.x, n.y, m_Width, "psiEyz1");
        psiEyz2.init(n.x, n.y, m_Width, "psiEyz2");

        psiEzx1.init(m_Width, n.y, n.z, "psiEzx1");
        psiEzx2.init(m_Width, n.y, n.z, "psiEzx2");
        psiEzy1.init(n.x, m_Width, n.z, "psiEzy1");
        psiEzy2.init(n.x, m_Width, n.z, "psiEzy2");

        psiHxy1.init(n.x, m_Width, n.z, "psiHxy1");
        psiHxy2.init(n.x, m_Width, n.z, "psiHxy2");
        psiHxz1.init(n.x, n.y, m_Width, "psiHxz1");
        psiHxz2.init(n.x, n.y, m_Width, "psiHxz2");

        psiHyx1.init(m_Width, n.y, n.z, "psiHyx1");
        psiHyx2.init(m_Width, n.y, n.z, "psiHyx2");
        psiHyz1.init(n.x, n.y, m_Width, "psiHyz1");
        psiHyz2.init(n.x, n.y, m_Width, "psiHyz2");

        psiHzx1.init(m_Width, n.y, n.z, "psiHzx1");
        psiHzx2.init(m_Width, n.y, n.z, "psiHzx2");
        psiHzy1.init(n.x, m_Width, n.z, "psiHzy1");
        psiHzy2.init(n.x, m_Width, n.z, "psiHzy2");
    }
    /*
        We also generate the usual gradings for the PML
    */
    Log::Info("Generating PML gradings.");
    this->generate(width, size.x, epsilon);

    m_IsInitialized = true;
}

void ConvolutionPmlBoundary::bakeUpdateCoefs(UpdateCoefficients& coefs) {
    if (m_IsInitialized)
        Log::Info("Baking CPML grading to update coefficients.");
    else
        Log::Error("Baking update coefficients before PML initialization.");

#pragma omp parallel for
    for (std::size_t i = 0; i < m_Width - 1; i++) {
        coefs.Hx[i] /= kappaH[i];
        coefs.Hx[n.x - 2 - i] /= kappaH[i];
        coefs.Hy[i] /= kappaH[i];
        coefs.Hy[n.y - 2 - i] /= kappaH[i];

#ifndef Z_PERIODIC
        /*
        If modelling space is periodic along Z,
        there is no need for grading in that direction.
        */
        coefs.Hz[i] /= kappaH[i];
        coefs.Hz[n.z - 2 - i] /= kappaH[i];
#endif
    }//END For H grading

#pragma omp parallel for
    for (std::size_t i = 0; i < m_Width; i++) {
        coefs.Ex[i] = fac_cfl / kappaE[i];
        coefs.Ex[n.x - 1 - i] = fac_cfl / kappaE[i];
        coefs.Ey[i] = fac_cfl / kappaE[i];
        coefs.Ey[n.y - 1 - i] = fac_cfl / kappaE[i];
#ifndef Z_PERIODIC
        coefs.Ez[i] = fac_cfl / kappaE[i];
        coefs.Ez[n.z - 1 - i] = fac_cfl / kappaE[i];
#endif
    }//END For E grading
}

void ConvolutionPmlBoundary::updateAndApplyH(const VectorField& E, VectorField& H, const UpdateCoefficients& deno)
{
    /*
        PML for H.x -- y direction
    */
#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 0; j < getWidth() - 1; j++) {
            for (std::size_t k = 0; k < n.z; k++) {
                psiHxy1(i, j, k) = bH(0, i, j) * psiHxy1(i, j, k)
                    + fac_cfl*cH(0, i, j) * (E.z(i, j, k) - E.z(i, j + 1, k));
                H.x(i, j, k) += DupB*psiHxy1(i, j, k);
                std::size_t jj = n.y - 2 - j;
                psiHxy2(i, j, k) = bH(0, i, j) * psiHxy2(i, j, k)
                    + fac_cfl*cH(0, i, j) * (E.z(i, jj, k) - E.z(i, jj + 1, k));
                H.x(i, jj, k) += DupB*psiHxy2(i, j, k);
            }
        }
    }

    /**
    *  PML for H.x -- z direction
    *
    */
#ifndef Z_PERIODIC
#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 0; k < getWidth() - 1; k++) {
                psiHxz1(i, j, k) = bH(0, i, k) * psiHxz1(i, j, k)
                    + fac_cfl*cH(0, i, k) * (E.y(i, j, k + 1) - E.y(i, j, k));
                H.x(i, j, k) += DupB*psiHxz1(i, j, k);
                std::size_t kk = n.z - 2 - k;
                psiHxz2(i, j, k) = bH(0, i, k) * psiHxz2(i, j, k)
                    + fac_cfl*cH(0, i, k) * (E.y(i, j, kk + 1) - E.y(i, j, kk));
                H.x(i, j, kk) += DupB*psiHxz2(i, j, k);
            }
        }
    }
#else
    for (std::size_t i = 1; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            H.x(i, j, n.z-1) += ((E.y(i, j, 0) - E.y(i, j, n.z-1))*deno.Hz[n.z-1]
                - (E.z(i, j + 1, n.z-1) - E.z(i, j, n.z-1))*deno.Hy[j]);
        }
        H.x(i, n.y - 1, n.z - 1) += ((E.y(i, n.y - 1, 0) - E.y(i, n.y - 1, n.z - 1))*deno.Hz[n.z - 1]
            /*- (E.z(i, 0, n.z - 1) - E.z(i, n.y - 1, n.z - 1))*deno.Hy[n.y - 1]*/);
    }
#endif

    /*
        PML for H.y -- x direction
    */
#pragma omp parallel for
    for (std::size_t i = 0; i < getWidth() - 1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 0; k < n.z; k++) {
                //c PML inf
                psiHyx1(i, j, k) = bH(0, 1, i) * psiHyx1(i, j, k) + fac_cfl*cH(0, 1, i) * (E.z(i + 1, j, k) - E.z(i, j, k));
                H.y(i, j, k) += DupB*psiHyx1(i, j, k);

                std::size_t iI = n.x - i - 2;
                psiHyx2(i, j, k) = bH(0, 1, i) * psiHyx2(i, j, k) + fac_cfl*cH(0, 1, i) * (E.z(iI + 1, j, k) - E.z(iI, j, k));
                H.y(iI, j, k) += DupB*psiHyx2(i, j, k);
            }
        }
    }

    /*
        PML for H.y -- z direction
    */
#ifndef Z_PERIODIC
#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 0; k < getWidth() - 1; k++) {
                //c PML inf
                psiHyz1(i, j, k) = bH(0, i, k) * psiHyz1(i, j, k) + fac_cfl*cH(0, i, k) * (E.x(i, j, k) - E.x(i, j, k + 1));
                H.y(i, j, k) += DupB*psiHyz1(i, j, k);

                std::size_t kk = n.z - k - 2;
                psiHyz2(i, j, k) = bH(0, i, k) * psiHyz2(i, j, k) + fac_cfl*cH(0, i, k) * (E.x(i, j, kk) - E.x(i, j, kk + 1));
                H.y(i, j, kk) += DupB*psiHyz2(i, j, k);
            }
        }
    }
#else
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y; j++) {
            H.y(i, j, n.z-1) += ((E.z(i + 1, j, n.z-1) - E.z(i, j, n.z-1))*deno.Hx[i]
                                       -(E.x(i, j, 0) - E.x(i, j, n.z-1))*deno.Hz[n.z-1]);
        }
}
#endif
    /*
    * PML pour H.y dans la direction des x
    *
    */
#pragma omp parallel for
    for (std::size_t i = 0; i < getWidth() - 1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 1; k < n.z; k++) {
                psiHzx1(i, j, k) = bH(0, 1, i) * psiHzx1(i, j, k) + fac_cfl*cH(0, 1, i) * (E.y(i, j, k) - E.y(i + 1, j, k));
                H.z(i, j, k) += DupB*psiHzx1(i, j, k);

                std::size_t iI = n.x - i - 2;
                psiHzx2(i, j, k) = bH(0, 1, i) * psiHzx2(i, j, k) + fac_cfl*cH(0, 1, i) * (E.y(iI, j, k) - E.y(iI + 1, j, k));
                H.z(iI, j, k) += DupB*psiHzx2(i, j, k);
            }
        }
    }

    /**
    *  PML for H.z -- y direction
    *
    */
#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 0; j < getWidth() - 1; j++) {
            for (std::size_t k = 1; k < n.z-1; k++) {
                psiHzy1(i, j, k) = bH(0, i, j) * psiHzy1(i, j, k)
                    + fac_cfl*cH(0, i, j) * (E.x(i, j + 1, k) - E.x(i, j, k));
                H.z(i, j, k) += DupB*psiHzy1(i, j, k);
                std::size_t jj = n.y - 2 - j;
                psiHzy2(i, j, k) = bH(0, i, j) * psiHzy2(i, j, k)
                    + fac_cfl*cH(0, i, j) * (E.x(i, jj + 1, k) - E.x(i, jj, k));
                H.z(i, jj, k) += DupB*psiHzy2(i, j, k);
            }
        }
    }

}

void ConvolutionPmlBoundary::updateAndApplyE(VectorField & E, const VectorField & H, const Field<real>& EPS, const UpdateCoefficients& deno)
{
    Vec3<std::size_t> n = E.getSize();
    /*
    * PML in the  y - direction
    *
    */

#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 1; j < getWidth(); j++) {
            for (std::size_t k = 1; k < n.z; k++) {
                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiExy1(i, j, k) = bE(0, i, j) * psiExy1(i, j, k) + fac_cfl*cE(0, i, j) * (H.z(i, j, k) - H.z(i, j - 1, k));
                // Apply the CPML std::size_tegral
                E.x(i, j, k) += CupB*psiExy1(i, j, k);

                std::size_t jj = n.y - j - 1;
                CupB = 1.0 / EPS(i, jj, k);
                psiExy2(i, j, k) = bE(0, i, j) * psiExy2(i, j, k) + fac_cfl*cE(0, i, j) * (H.z(i, jj, k) - H.z(i, jj - 1, k));
                E.x(i, jj, k) += CupB*psiExy2(i, j, k);

            }
        }
    }

    /*
    * PML in the  z - direction
    *
    */
#ifndef Z_PERIODIC
#pragma omp parallel for
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 1; j < n.y-1; j++) {
            for (std::size_t k = 1; k < getWidth(); k++) {
                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiExz1(i, j, k) = bE(0, i, k) * psiExz1(i, j, k) + fac_cfl*cE(0, i, k) * (H.y(i, j, k - 1) - H.y(i, j, k));
                // Apply the CPML std::size_tegral
                E.x(i, j, k) += CupB*psiExz1(i, j, k);

                std::size_t kk = n.z - k - 1;
                CupB = 1.0 / EPS(i, j, kk);
                psiExz2(i, j, k) = bE(0, i, k) * psiExz2(i, j, k) + fac_cfl*cE(0, i, k) * (H.y(i, j, kk - 1) - H.y(i, j, kk));
                E.x(i, j, kk) += CupB*psiExz2(i, j, k);

            }
        }
    }
#else
    //PBC for z direction
    for (std::size_t i = 0; i < n.x-1; i++) {
        for (std::size_t j = 1; j < n.y; j++) {
            E.x(i, j, 0) += ((H.z(i, j, 0) - H.z(i, j - 1, 0))*deno.Ey[j]
                - (H.y(i, j, 0) - H.y(i, j, n.z - 1))*deno.Ez[0]) / EPS(i, j, 0);
        }
       
            E.x(i, 0, 0) += (/*(H.z(i, 0, 0) - H.z(i, n.y-1, 0))*deno.Ey[0]*/
                - (H.y(i, 0, 0) - H.y(i, 0, n.z - 1))*deno.Ez[0]) / EPS(i, 0, 0);
    }
#endif

    // PML in the x -direction
#pragma omp parallel for
    for (std::size_t i = 1; i < getWidth(); i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 1; k < n.z; k++) {

                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiEyx1(i, j, k) = bE(0, 1, i) * psiEyx1(i, j, k) + fac_cfl*cE(0, 1, i) * (H.z(i - 1, j, k) - H.z(i, j, k));
                // Apply the CPML std::size_tegral
                E.y(i, j, k) += CupB*psiEyx1(i, j, k);

                std::size_t III = n.x - i - 1;
                CupB = 1.0 / EPS(III, j, k);
                psiEyx2(i, j, k) = bE(0, 1, i) * psiEyx2(i, j, k) + fac_cfl*cE(0, 1, i) * (H.z(III - 1, j, k) - H.z(III, j, k));
                E.y(III, j, k) += CupB*psiEyx2(i, j, k);

            }
        }
    }

/*
    PML for E.y -- z direction
*/
#ifndef Z_PERIODIC
#pragma omp parallel for
    for (std::size_t i = 1; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y-1; j++) {
            for (std::size_t k = 1; k < getWidth(); k++) {
                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiEyz1(i, j, k) = bE(0, i, k) * psiEyz1(i, j, k) + fac_cfl*cE(0, i, k) * (H.x(i, j, k) - H.x(i, j, k - 1));
                // Apply the CPML std::size_tegral
                E.y(i, j, k) += CupB*psiEyz1(i, j, k);

                std::size_t kk = n.z - k - 1;
                CupB = 1.0 / EPS(i, j, k);
                psiEyz2(i, j, k) = bE(0, i, k) * psiEyz2(i, j, k) + fac_cfl*cE(0, i, k) * (H.x(i, j, kk) - H.x(i, j, kk - 1));
                E.y(i, j, kk) += CupB*psiEyz2(i, j, k);

            }
        }
    }
#else
    // PBC for z direction
    for (std::size_t i = 1; i < n.x-1; i++) {
        for (std::size_t j = 0; j < n.y; j++) {
            E.y(i, j, 0) += ((H.x(i, j, 0) - H.x(i, j, n.z - 1))*deno.Ez[0]
                    -(H.z(i, j, 0) - H.z(i - 1, j, 0))*deno.Ex[i]) / EPS(i, j, 0);
        }
    }

#endif

    /*
    * PML in the  x - direction
    *
    */
#pragma omp parallel for
    for (std::size_t i = 1; i < getWidth(); i++) {
        for (std::size_t j = 1; j < n.y-1; j++) {
            for (std::size_t k = 0; k < n.z; k++) {
                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiEzx1(i, j, k) = bE(0, 1, i) * psiEzx1(i, j, k) + fac_cfl*cE(0, 1, i) * (H.y(i, j, k) - H.y(i - 1, j, k));
                // Apply the CPML std::size_tegral
                E.z(i, j, k) += CupB*psiEzx1(i, j, k);

                std::size_t III = n.x - i - 1;
                CupB = 1.0 / EPS(III, j, k);
                psiEzx2(i, j, k) = bE(0, 1, i) * psiEzx2(i, j, k) + fac_cfl*cE(0, 1, i) * (H.y(III, j, k) - H.y(III - 1, j, k));
                E.z(III, j, k) += CupB*psiEzx2(i, j, k);

            }
        }
    }

    /*
    * PML in the  y - direction
    *
    */
#pragma omp parallel for
    for (std::size_t i = 1; i < n.x - 1; i++) {
        for (std::size_t j = 1; j < getWidth(); j++) {
            for (std::size_t k = 0; k < n.z; k++) {
                real CupB = 1.0 / EPS(i, j, k);
                // Update the std::size_tegral for i=0-side CPML
                psiEzy1(i, j, k) = bE(0, i, j) * psiEzy1(i, j, k) + fac_cfl*cE(0, i, j) * (H.x(i, j - 1, k) - H.x(i, j, k));
                // Apply the CPML std::size_tegral
                E.z(i, j, k) += CupB*psiEzy1(i, j, k);

                std::size_t jj = n.y - j - 1;
                CupB = 1.0 / EPS(i, jj, k);
                psiEzy2(i, j, k) = bE(0, i, j) * psiEzy2(i, j, k) + fac_cfl*cE(0, i, j) * (H.x(i, jj - 1, k) - H.x(i, jj, k));
                E.z(i, jj, k) += CupB*psiEzy2(i, j, k);

            }
        }
    }

}
