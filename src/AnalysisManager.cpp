#include "AnalysisManager.h"

AnalysisManager::AnalysisManager()
{}

AnalysisManager::~AnalysisManager() = default;

std::size_t AnalysisManager::getHorizontalPlaneSurface()
{
    return horiz_size.x * horiz_size.y;
}

void AnalysisManager::init(const real fMin, const real fMax, const std::size_t n,
                           const FlowPlaneProbe &h_flow_plane, const FlowPlaneProbe &l_flow_plane)
{
    real width = fMax - fMin;
    real step = width / (real)(n - 1);

    m_Frequencies.assign(n, 0.0);

    // 1D measurement points.
    E2DyplanR_1D.assign(n, 0.0);
    H2DzplanR_1D.assign(n, 0.0);
    H2DyplanR_1D.assign(n, 0.0);
    E2DzplanR_1D.assign(n, 0.0);
    SourceEz.assign(n, 0.0);
    SourceHy.assign(n, 0.0);

    // Reflectance and tranmittance
    m_Iref.assign(n, 0.0);
    m_Itran.assign(n, 0.0);
    m_ILat.assign(n, 0.0);
    m_ISrc.assign(n, 0.0);

    n_Freq = n;
    flow_R = h_flow_plane.offset;
    flow_L = l_flow_plane.offset;

    horiz_begin = h_flow_plane.position;
    horiz_size = h_flow_plane.size;

    lateral_begin = l_flow_plane.position;
    lateral_size = l_flow_plane.size;

    for (std::size_t f = 0; f < n; f++)
    {
        m_Frequencies[f] = fMin + step * f;
    }

    E2DyplanR.init(n, horiz_size.x, horiz_size.y, "E2DyplanR");
    E2DzplanR.init(n, horiz_size.x, horiz_size.y, "E2DzplanR");
    E2DyplanT.init(n, horiz_size.x, horiz_size.y, "E2DyplanT");
    E2DzplanT.init(n, horiz_size.x, horiz_size.y, "E2DzplanT");
    H2DyplanR.init(n, horiz_size.x, horiz_size.y, "H2DyplanR");
    H2DzplanR.init(n, horiz_size.x, horiz_size.y, "H2DzplanR");
    H2DyplanT.init(n, horiz_size.x, horiz_size.y, "H2DyplanT");
    H2DzplanT.init(n, horiz_size.x, horiz_size.y, "H2DzplanT");

    E2DxplanLR.init(n, lateral_size.x, lateral_size.y, "E2DxplanLR");
    E2DzplanLR.init(n, lateral_size.x, lateral_size.y, "E2DzplanLR");
    H2DxplanLR.init(n, lateral_size.x, lateral_size.y, "H2DxplanLR");
    H2DzplanLR.init(n, lateral_size.x, lateral_size.y, "H2DzplanLR");

    E2DxplanLL.init(n, lateral_size.x, lateral_size.y, "E2DxplanLL");
    E2DzplanLL.init(n, lateral_size.x, lateral_size.y, "E2DzplanLL");
    H2DxplanLL.init(n, lateral_size.x, lateral_size.y, "H2DxplanLL");
    H2DzplanLL.init(n, lateral_size.x, lateral_size.y, "H2DzplanLL");

    /*
        To prevent watchdog from allocating bits by bits.
    */

    m_Watchdog.reserve(3000);

    /*
		Allocating space for snapshots registered
	*/

    for (std::size_t p = 0; p < m_poynting_snapshots.size(); p++)
    {
        if (m_poynting_snapshots[p].axis == SnapshotAxis::X)
            m_poynting_snapshots[p].init(m_poynting_snapshots[p].frequencies.size(),
                                         m_poynting_snapshots[p].p_probed_field->size.y,
                                         m_poynting_snapshots[p].p_probed_field->size.z,
                                         "Field snapshot yz");
        else if (m_poynting_snapshots[p].axis == SnapshotAxis::Y)
            m_poynting_snapshots[p].init(m_poynting_snapshots[p].frequencies.size(),
                                         m_poynting_snapshots[p].p_probed_field->size.x,
                                         m_poynting_snapshots[p].p_probed_field->size.z,
                                         "Field snapshot xz");
        else if (m_poynting_snapshots[p].axis == SnapshotAxis::Z)
            m_poynting_snapshots[p].init(m_poynting_snapshots[p].frequencies.size(),
                                         m_poynting_snapshots[p].p_probed_field->size.x,
                                         m_poynting_snapshots[p].p_probed_field->size.y,
                                         "Field snapshot xy");
        else
            Log::Error("Undetermined snapshot axis, failed init.");
    }
}

void AnalysisManager::update(const VectorField &E, const VectorField &H, const std::size_t T)
{
    /*
        This function needs to acces all field elements between specified boundaries.
        Using measurement plane inside CPML can cause unexpected behaviour.
    */

    // Reference to modelling domain size.
    const Vec3<std::size_t> &domain_size = E.getSize();

#   pragma omp parallel for
    for (std::size_t f = 0; f < n_Freq; f++)
    {
        /*
            The kernels for Dicrete Fourier Transform update
            The kernel for E field is shifted by half a temporal std::size_t to respect Yee's time-stepping.
        */

        const complex kernTFE = std::exp(Constants::Icomp * Constants::twopi * m_Frequencies[f] * static_cast<real>(T));
        const complex kernTFH = std::exp(Constants::Icomp * Constants::twopi * m_Frequencies[f] * static_cast<real>(T + .5));

        std::size_t Nplan = 0;

#       pragma omp parallel for
        for (std::size_t j = horiz_begin.x; j < horiz_begin.x + horiz_size.x; j++)
        {

            for (std::size_t k = horiz_begin.y; k < horiz_begin.y + horiz_size.y; k++)
            {

                const std::size_t jj = j - horiz_begin.x;
                const std::size_t kk = k - horiz_begin.y;
                /*
                    Frontal collection plane - E for little i
                */

                Nplan = flow_R;

                // Ey(i,j+.5,k)
                E2DyplanR(f, jj, kk) += kernTFE * (+E.y(Nplan, j, k) + E.y(Nplan, j - 1, k)) / static_cast<real>(2.0);

                // Ez(i,j,k+.5)
                E2DzplanR(f, jj, kk) += kernTFE * (+E.z(Nplan, j, k) + E.z(Nplan, j, k - 1)) / static_cast<real>(2.0);

                /*
                    Back collection plane - E for little i
                */

                Nplan = domain_size.x - flow_R + 1;

                // Ey(i,j+.5,k)
                E2DyplanT(f, jj, kk) += kernTFE * (+E.y(Nplan, j, k) + E.y(Nplan, j - 1, k)) / static_cast<real>(2.0);

                // Ez(i,j,k+.5)
                E2DzplanT(f, jj, kk) += kernTFE * (+E.z(Nplan, j, k) + E.z(Nplan, j, k - 1)) / static_cast<real>(2.0);

                /*
                    Front collection plane - E for little i
                */

                Nplan = flow_R;

                // Hy(i+.5,j,k+.5)
                H2DyplanR(f, jj, kk) += kernTFH * (+H.y(Nplan, j, k) + H.y(Nplan - 1, j, k) + H.y(Nplan, j, k - 1) + H.y(Nplan - 1, j, k - 1)) / static_cast<real>(4.0);

                // Hz(i+.5,j+.5,k)
                H2DzplanR(f, jj, kk) += kernTFH * (+H.z(Nplan, j, k) + H.z(Nplan - 1, j, k) + H.z(Nplan, j - 1, k) + H.z(Nplan - 1, j - 1, k)) / static_cast<real>(4.0);

                /*
                    Back collection plane - H for big i
                */
                Nplan = domain_size.x - flow_R + 1;

                // Hy(i+.5,j,k+.5)
                H2DyplanT(f, jj, kk) += kernTFH * (+H.y(Nplan, j, k) + H.y(Nplan - 1, j, k) + H.y(Nplan, j, k - 1) + H.y(Nplan - 1, j, k - 1)) / static_cast<real>(4.0);

                // Hz(i+.5,j+.5,k)
                H2DzplanT(f, jj, kk) += kernTFH * (+H.z(Nplan, j, k) + H.z(Nplan - 1, j, k) + H.z(Nplan, j - 1, k) + H.z(Nplan - 1, j - 1, k)) / static_cast<real>(4.0);
            }
        }
#       pragma omp parallel for
        for (std::size_t i = lateral_begin.x; i < lateral_begin.x + lateral_size.x; i++)
        {

            for (std::size_t k = lateral_begin.y; k < lateral_begin.y + lateral_size.y; k++)
            {

                const std::size_t ii = i - lateral_begin.x;
                const std::size_t kk = k - lateral_begin.y;

                /*
                    Lateral collection plane - E for little j
                */

                Nplan = flow_L;

                // Ex(i+.5,j,k)
                E2DxplanLL(f, ii, kk) += kernTFE * (+E.x(i, Nplan, k) + E.x(i - 1, Nplan, k)) / static_cast<real>(2.0);

                // Ez(i,j,k+.5)
                E2DzplanLL(f, ii, kk) += kernTFE * (+E.z(i, Nplan, k) + E.z(i, Nplan, k - 1)) / static_cast<real>(2.0);

                /*
                    Lateral collection plane - E for big j
                */

                Nplan = domain_size.y - flow_L + 1;

                // Ex(i+.5,j,k)
                E2DxplanLR(f, ii, kk) += kernTFE * (+E.x(i, Nplan, k) + E.x(i - 1, Nplan, k)) / static_cast<real>(2.0);

                // Ez(i,j,k+.5)
                E2DzplanLR(f, ii, kk) += kernTFE * (+E.z(i, Nplan, k) + E.z(i, Nplan, k - 1)) / static_cast<real>(2.0);

                /*
                    Lateral collection plane - H for little j
                */

                Nplan = flow_L;

                // Hx(i,j+.5,k+.5)
                H2DxplanLL(f, ii, kk) += kernTFH * (+H.x(i, Nplan, k) + H.x(i, Nplan - 1, k) + H.x(i, Nplan, k - 1) + H.x(i, Nplan - 1, k - 1)) / static_cast<real>(4.0);

                // Hz(i+.5,j+.5,k)
                H2DzplanLL(f, ii, kk) += kernTFH * (+H.z(i, Nplan, k) + H.z(i - 1, Nplan, k) + H.z(i, Nplan - 1, k) + H.z(i - 1, Nplan - 1, k)) / static_cast<real>(4.0);

                /*
                    Lateral collection plane - H for big j
                */

                Nplan = domain_size.y - flow_L + 1;

                // Hx(i,j+.5,k+.5)
                H2DxplanLR(f, ii, kk) -= kernTFH * (+H.x(i, Nplan, k) + H.x(i, Nplan - 1, k) + H.x(i, Nplan, k - 1) + H.x(i, Nplan - 1, k - 1)) / static_cast<real>(4.0);

                // Hz(i+.5,j+.5,k)
                H2DzplanLR(f, ii, kk) -= kernTFH * (+H.z(i, Nplan, k) + H.z(i - 1, Nplan, k) + H.z(i, Nplan - 1, k) + H.z(i - 1, Nplan - 1, k)) / static_cast<real>(4.0);

            } // For k
        }     // For j
    }         // For f
}

void AnalysisManager::poyntingVector(std::string sim_id)
{
    Log::Hbar();
    Log::Info("Now computing Poynting vector flux.");

    /*
        Calculating Poynting vector S_m - Reflexion plane.
    */
    for (std::size_t f = 0; f < m_Frequencies.size(); f++)
    {
        m_Iref[f] = 0.0;
        for (std::size_t j = 0; j < horiz_size.x; j++)
        {
            for (std::size_t k = 0; k < horiz_size.y; k++)
            {
                // <S_m|x> = (E_y.H_z*)-(E_z.H_y*)
                m_Iref[f] += (
                              - std::conj(E2DyplanR(f, j, k))// - E2DyplanR_1D[f] * indice_IN)
                              *          (H2DzplanR(f, j, k))// - H2DzplanR_1D[f] * indice_IN)
                              + std::conj(E2DzplanR(f, j, k))// - E2DzplanR_1D[f] * indice_IN)
                              *          (H2DyplanR(f, j, k))// - H2DyplanR_1D[f] * indice_IN)
                             ).real();
            }
        }
    }

    /*
        Calculating Poynting vector S_m - "Transmission" plane
    */

    for (std::size_t f = 0; f < m_Frequencies.size(); f++)
    {
        m_Itran[f] = 0.0;
        for (std::size_t j = 0; j < horiz_size.x; j++)
        {
            for (std::size_t k = 0; k < horiz_size.y; k++)
            {
                // <S_m|x> = (E_y.H_z*)-(E_z.H_y*)
                m_Itran[f] += (
                                - std::conj(E2DyplanT(f, j, k))
                                *           H2DzplanT(f, j, k)
                                + std::conj(E2DzplanT(f, j, k))
                                *           H2DyplanT(f, j, k)
                              ).real();
            }
        }
    }

    /*
        Calculating Poynting vector S_m - "Lateral" right plane
    */
    for (std::size_t f = 0; f < m_Frequencies.size(); f++)
    {
        for (std::size_t i = 0; i < lateral_size.x; i++)
        {
            for (std::size_t k = 0; k < lateral_size.y; k++)
            {
                // <S_m|y> = (E_x.H_z*)-(E_z.H_x*)
                m_ILat[f] += (
                              + std::conj(E2DxplanLR(f, i, k))
                              *           H2DzplanLR(f, i, k)
                              - std::conj(E2DzplanLR(f, i, k)) 
                              *           H2DxplanLR(f, i, k)
                             ).real();
            }
        }
    }

    /*
	Calculating Poynting vector S_m - "Lateral" left plane
	*/
	for (std::size_t f = 0; f < m_Frequencies.size(); f++)
	{
		for (std::size_t i = 0; i < lateral_size.x; i++)
		{
			for (std::size_t k = 0; k < lateral_size.y; k++)
			{
				// <S_m|y> = (E_x.H_z*)-(E_z.H_x*)
				m_ILat[f] += (
					          + std::conj(E2DxplanLL(f, i, k))
					          *           H2DzplanLL(f, i, k)
					          - std::conj(E2DzplanLL(f, i, k))
					          *           H2DxplanLL(f, i, k)
					         ).real();
			}
		}
	}

    /*
        Calculating Poynting vector S_m - Source 1D plane
    */

    for (std::size_t f = 0; f < m_Frequencies.size(); f++)
    {
        m_ISrc[f] -= (std::conj(SourceEz[f]) * SourceHy[f]).real();
    }

    Log::Info("Saving flux spectrum.");
    {
        std::ofstream f("io/" + sim_id + "/spectral.reflexion.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_Iref.begin(), m_Iref.end(), output_iterator);
        f.close();
    }
    {
        std::ofstream f("io/" + sim_id + "/spectral.transmission.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_Itran.begin(), m_Itran.end(), output_iterator);
        f.close();
    }
    {
        std::ofstream f("io/" + sim_id + "/spectral.lateral.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_ILat.begin(), m_ILat.end(), output_iterator);
        f.close();
    }
    Log::Info("Saving watchdog data.");
    {
        std::ofstream f;
        f.open("io/" + sim_id + "/watchdog.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_Watchdog.begin(), m_Watchdog.end(), output_iterator);
        f.close();
    }
    {
        std::ofstream f("io/" + sim_id + "/spectral.normalized_frequency.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_Frequencies.begin(), m_Frequencies.end(), output_iterator);
        f.close();
    }
    {
        std::ofstream f("io/" + sim_id + "/spectral.source.csv");
        std::ostream_iterator<real> output_iterator(f, "\n");
        std::copy(m_ISrc.begin(), m_ISrc.end(), output_iterator);
        f.close();
    }

    {
        for (std::size_t p = 0; p < m_poynting_snapshots.size(); ++p)
        {

            for (std::size_t i = 0; i < m_poynting_snapshots[p].size.x; ++i)
            {
                std::cout << "Saving snapshot @"
                          << "io/" + sim_id + "snapshot." + std::to_string(p) + "_" + std::to_string(i) + ".csv" << std::endl;
                std::ofstream fr("io/" + sim_id + "/snapshot." + std::to_string(p) + "_" + std::to_string(i) + ".real.csv");
                std::ofstream fi("io/" + sim_id + "/snapshot." + std::to_string(p) + "_" + std::to_string(i) + ".imag.csv");
                for (std::size_t j = 0; j < m_poynting_snapshots[p].size.y; ++j)
                {
                    for (std::size_t k = 0; k < m_poynting_snapshots[p].size.z; ++k)
                    {

                        fr << m_poynting_snapshots[p](i, j, k).real();
                        fi << m_poynting_snapshots[p](i, j, k).imag();

                        if (k + 1 < m_poynting_snapshots[p].size.z)
                        {
                            fr << ",";
                            fi << ",";
                        }
                    }
                    fr << std::endl;
                    fi << std::endl;
                }
                fi.close();
                fr.close();
            }
        }
    }
    Log::Info("Save complete.");
}

void AnalysisManager::updatePoyntingSnapshot(const std::size_t T)
{

    for (std::size_t p = 0; p < m_poynting_snapshots.size(); p++)
    {

        for (std::size_t f = 0; f < m_poynting_snapshots[p].frequencies.size(); f++)
        {
            const real freq = m_poynting_snapshots[p].frequencies[f];
            const complex kernTFE = std::exp(Constants::Icomp * Constants::twopi * freq * static_cast<real>(T));
            const complex kernTFH = std::exp(Constants::Icomp * Constants::twopi * freq * static_cast<real>(T + .5));

            const Vec3<std::size_t> size = m_poynting_snapshots[p].p_probed_field->size;

            std::size_t level = m_poynting_snapshots[p].level;

            if (m_poynting_snapshots[p].axis == SnapshotAxis::Z)
                for (std::size_t i = 0; i < size.x; i++)
                    for (std::size_t j = 0; j < size.y; j++)
                    {
                        // We first compute the fourier transform at this coordinate (i,j,k)
                        if (m_poynting_snapshots[p].type == SnapshotFieldType::E)
                            m_poynting_snapshots[p](f, i, j) += kernTFE * +m_poynting_snapshots[p].p_probed_field->operator()(i, j, level);
                        else
                            m_poynting_snapshots[p](f, i, j) += kernTFH * +m_poynting_snapshots[p].p_probed_field->operator()(i, j, level);
                    }
            if (m_poynting_snapshots[p].axis == SnapshotAxis::X)
                for (std::size_t j = 0; j < size.y; j++)
                    for (std::size_t k = 0; k < size.z; k++)
                    {
                        // We first compute the fourier transform at this coordinate (i,j,k)
                        if (m_poynting_snapshots[p].type == SnapshotFieldType::E)
                            m_poynting_snapshots[p](f, j, k) += kernTFE * +m_poynting_snapshots[p].p_probed_field->operator()(level, j, k);
                        else
                            m_poynting_snapshots[p](f, j, k) += kernTFH * +m_poynting_snapshots[p].p_probed_field->operator()(level, j, k);
                    }

            if (m_poynting_snapshots[p].axis == SnapshotAxis::Y)
                for (std::size_t i = 0; i < size.x; i++)
                    for (std::size_t k = 0; k < size.z; k++)
                    {
                        // We first compute the fourier transform at this coordinate (i,j,k)
                        if (m_poynting_snapshots[p].type == SnapshotFieldType::E)
                            m_poynting_snapshots[p](f, i, k) += kernTFE * +m_poynting_snapshots[p].p_probed_field->operator()(i, level, k);
                        else
                            m_poynting_snapshots[p](f, i, k) += kernTFH * +m_poynting_snapshots[p].p_probed_field->operator()(i, level, k);
                    }
        }
    }
}

void AnalysisManager::registerSnapshot(SnapshotAxis axis, SnapshotFieldType type, std::size_t level, std::vector<real> frequency, Field<real> *field_ref)
{
    SnapshotField<complex> temp;
    temp.level = level;
    temp.frequencies = frequency;
    temp.axis = axis;
    temp.type = type;
    temp.p_probed_field = field_ref;
    m_poynting_snapshots.push_back(temp);
}

real AnalysisManager::updateWatchdog(std::size_t T, const Field<real> &field)
{

    real temp = 0.0;
    for (std::size_t i = 0; i < field.size.x; i++)
        for (std::size_t j = 0; j < field.size.y; j++)
            for (std::size_t k = 0; k < field.size.z; k++)
            {
                temp += std::abs(field(i, j, k));
            }
    m_Watchdog.push_back(temp);
    //Log::Warning("Performing domain-wide sum : " + std::to_string(temp));
    return m_Watchdog[m_Watchdog.size() - 1];
}

void AnalysisManager::updateSource(const AuxilaryYeeDomain &ad, const std::size_t T)
{
    Vec2<real> source = ad.getSource(T);

    for (std::size_t f = 0; f < n_Freq; f++)
    {
        complex kernTFE = std::exp(Constants::Icomp * static_cast<real>(2.) * Constants::pi * m_Frequencies[f] * static_cast<real>(T));
        complex kernTFH = std::exp(Constants::Icomp * static_cast<real>(2.) * Constants::pi * m_Frequencies[f] * static_cast<real>(T + .5));

        //std::size_t Nplan = flow_R;

        SourceEz[f] += source.x * kernTFE;
        SourceHy[f] += source.y * kernTFH;
    }
}
