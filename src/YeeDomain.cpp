#include "./YeeDomain.h"
#include <omp.h>
/*
    YeeDomain
    FDTD Core time-stepping and fields container.
    YeeDomain is the backbone of any FDTD computation using Yee Grid.
*/

void YeeDomain::updateMagneticField()
{
    std::size_t i, j, k;
<<<<<<< HEAD
#   pragma omp parallel default(shared) private(i, j, k)
=======
#pragma omp parallel default(shared) private(i,j,k) 
>>>>>>> 42ed39149b4723c40eb695df0b17b5c7c4e89e3f
    {
#       pragma omp for schedule(dynamic)
        for (i = 0; i < m_Size.x - 1; i++)
        {
            for (j = 0; j < m_Size.y - 1; j++)
            {
                for (k = 0; k < m_Size.z - 1; k++)
                {
                    H.x(i, j, k) += (+(E.y(i, j, k + 1) - E.y(i, j, k)) * deno.Hz[k] + (E.z(i, j, k) - E.z(i, j + 1, k)) * deno.Hy[j]);
                } // For Hz k
            }     // For Hz j
        }         // For Hz i

#       pragma omp for schedule(dynamic)
        for (i = 0; i < m_Size.x - 1; i++)
        {
            for (j = 0; j < m_Size.y - 1; j++)
            {
                for (k = 0; k < m_Size.z - 1; k++)
                {
                    H.y(i, j, k) += (+(E.z(i + 1, j, k) - E.z(i, j, k)) * deno.Hx[i] + (E.x(i, j, k) - E.x(i, j, k + 1)) * deno.Hz[k]);
                } // For Hy k
            }     // For Hy j
        }         // For Hy i

#       pragma omp for schedule(dynamic)
        for (i = 0; i < m_Size.x - 1; i++)
        {
            for (j = 0; j < m_Size.y - 1; j++)
            {
                for (k = 1; k < m_Size.z; k++)
                {
                    H.z(i, j, k) += (+(E.x(i, j + 1, k) - E.x(i, j, k)) * deno.Hy[j] + (E.y(i, j, k) - E.y(i + 1, j, k)) * deno.Hx[i]);
                } // For Hz k
            }     // For Hz j
        }         // For Hz i
    }             // END OMP PARALLEL
}

void YeeDomain::updateElectricField()
{
    std::size_t i, j, k;
#pragma omp parallel default(shared) private(i,j,k)
    {
#       pragma omp for schedule(dynamic)
        for (i = 0; i < m_Size.x - 1; i++)
        {
            for (j = 1; j < m_Size.y - 1; j++)
            {
                for (k = 1; k < m_Size.z; k++)
                {
                    E.x(i, j, k) += (+(H.z(i, j, k) - H.z(i, j - 1, k)) * deno.Ey[j] + (H.y(i, j, k - 1) - H.y(i, j, k)) * deno.Ez[k]) / epsilon(i, j, k);
                } // For Hz k
            }     // For Hz j
        }         // For Hz i


#       pragma omp for schedule(dynamic)
        for (i = 1; i < m_Size.x - 1; i++)
        {
            for (j = 0; j < m_Size.y - 1; j++)
            {
                for (k = 1; k < m_Size.z; k++)
                {
                    E.y(i, j, k) += (+(H.x(i, j, k) - H.x(i, j, k - 1)) * deno.Ez[k] + (H.z(i - 1, j, k) - H.z(i, j, k)) * deno.Ex[i]) / epsilon(i, j, k);
                } // For Hz k
            }     // For Hz j
        }         // For Hz i


#       pragma omp for schedule(dynamic)
        for (i = 1; i < m_Size.x - 1; i++)
        {
            for (j = 1; j < m_Size.y - 1; j++)
            {
                for (k = 0; k < m_Size.z; k++)
                {
                    E.z(i, j, k) += (+(H.y(i, j, k) - H.y(i - 1, j, k)) * deno.Ex[i] + (H.x(i, j - 1, k) - H.x(i, j, k)) * deno.Ey[j]) / epsilon(i, j, k);
                } // For Hz k
            }     // For Hz j
        }         // For Hz i
    }
}


YeeDomain::YeeDomain(const Vec3<size_t>& size, const real& courant_number) : 
           m_Size(size),
           m_CourantNumber(courant_number)    
{
    Log::Hbar();
    Log::Info("YeeDomain initialization.");
    
    epsilon.init(m_Size, "Epsilon");
    E.init(m_Size, "E_field");
    H.init(m_Size, "H_field");
    deno.initAll(m_Size, m_CourantNumber);
}
