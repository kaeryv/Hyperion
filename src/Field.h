#pragma once
//#define ACCES_DEBUG

#include "utils/VectorUtils.h"

#include <vector>
#include <string>


template<typename Type>
class Field
{
public:
	Field():m_Size(0,0,0), m_data(nullptr)
    {

    }
    ~Field() {
        delete[] m_data;
    }
	explicit Field(Vec3<size_t> size): Field(size.x, size.y, size.z){}
	Field(size_t Nx, size_t Ny, size_t Nz) : m_Size{ Nx, Ny, Nz } 
    {

    }
    void alloc() {
        m_data = (Type*)calloc(m_Size.x * m_Size.y * m_Size.z, sizeof(Type));
    }
	void init(const Vec3<size_t>& s, std::string name) {
        m_Name = name;
        m_Size = s;
        alloc();
    }
    void init(const size_t nx, const size_t ny, const size_t nz, std::string name) {
        m_Name = name;
        m_Size = Vec3<size_t>{ nx, ny, nz };
        alloc();
    }
	void zeros() {
        fill(0.0);
    }
	void ones() {
        fill(1.0);
    }
	void fill(Type value) {
        std::fill_n(m_data, m_Size.x * m_Size.y * m_Size.z, value);
    }

	Type& operator()(const size_t x, const size_t y, const size_t z) noexcept {
#   ifdef ACCES_DEBUG
        if (x >= m_Size.x || y >= m_Size.y || z >= m_Size.z || x < 0 || y < 0 || z < 0) {
#           pragma omp critical
            std::cout << "Field& acces overflow ! @ " << m_Name << std::endl;
            std::cout << x << " as x " << y << " as y " << z << std::endl;
			std::cin.get();
            exit(1);
        }
#   endif
        return m_data[x * (m_Size.y * m_Size.z) + y * m_Size.z + z];
    }

	const Type& operator()(const size_t x, const size_t y, const size_t z) const noexcept {
#   ifdef ACCES_DEBUG
        if (x >= m_Size.x || y >= m_Size.y || z >= m_Size.z || x < 0 || y < 0 || z < 0) {
#           pragma omp critical
            std::cout << "const Field& acces overflow ! @ " << m_Name << std::endl;
            std::cout << x << " as x " << y << " as y " << z << std::endl;
			std::cin.get();
            exit(1);
        }
#   endif
        return m_data[x * (m_Size.y * m_Size.z) + y * m_Size.z + z];
    }

    Type& operator[](const std::size_t index) noexcept
    {
        return m_data[index];
    }
    
    const Type& operator[](const std::size_t index) const noexcept
    {
        return m_data[index];
    }

	Field& operator=(Type value) {
        fill(value);
        return (*this);
    }

    const Vec3<std::size_t>& size() const
    {
        return m_Size;
    }
public:
	Vec3<size_t> m_Size;
private:
	Type* m_data;
    std::string m_Name;
};

