#pragma once

#include "VectorField.h"
#include "Log.h"

/*
    UpdateCoefficients
    Structure containing dt/dx coefficients needed by global time-stepping.
*/
struct UpdateCoefficients
{
	real *Hx;
	real *Hy;
	real *Hz;
	real *Ex;
	real *Ey;
	real *Ez;

	Vec3<std::size_t> size;

	void initAll(const Vec3<std::size_t> &s, const real &value)
	{

		size = s;

		Hx = (real *)calloc(size.x, sizeof(real));
		Hy = (real *)calloc(size.y, sizeof(real));
		Hz = (real *)calloc(size.z, sizeof(real));
		Ex = (real *)calloc(size.x, sizeof(real));
		Ey = (real *)calloc(size.y, sizeof(real));
		Ez = (real *)calloc(size.z, sizeof(real));

		std::fill_n(Hx, size.x, value);
		std::fill_n(Hy, size.y, value);
		std::fill_n(Hz, size.z, value);
		std::fill_n(Ex, size.x, value);
		std::fill_n(Ey, size.y, value);
		std::fill_n(Ez, size.z, value);
	}
};

class YeeDomain
{
  public:
	// Fields update
	void updateMagneticField();
	void updateElectricField();

	// Contructors require size and courant number.
	YeeDomain(const Vec3<std::size_t> &size) : YeeDomain(size, .5) {}
	YeeDomain(const Vec3<std::size_t> &size, const real &courant_number);
	const Vec3<std::size_t> get_size() const { return m_Size; }
	// Acessors for dielectric data.
	Field<real> &getEpsilon() { return epsilon; }
	const Field<real> &getEpsilon() const { return epsilon; }

	~YeeDomain() {}

	VectorField E, H;

  protected:
	Field<real> epsilon;
	UpdateCoefficients deno;
	real m_CourantNumber;
	Vec3<std::size_t> m_Size;
};
