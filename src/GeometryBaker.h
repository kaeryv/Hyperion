#pragma once

#include "Geometry.h"
#include "Layer.h"

enum class BackgroundType
{
    UNIFORM,
    LAYERED
};

class GeometryBaker
{
  public:
    GeometryBaker();
    void appendGeometry(Geometry *g);
    void appendLayer(Layer *l);
    void setBackgroundEpsilon(real eps) { m_BackgroundEpsilon = eps; }
    void setBackgroundType(BackgroundType bt) { m_Type = bt; }
    void bakeGeometries(Field<real> &eps);
    std::vector<real> *getBackgroundVector();
    ~GeometryBaker();
    std::size_t getGeometriesCount() { return m_Geometries.size(); }

  private:
    std::vector<Geometry *> m_Geometries;
    std::vector<Layer *> m_Layers;
    std::vector<real> m_Background;
    BackgroundType m_Type;
    real m_BackgroundEpsilon;
};
