#include "EngineFoundry.h"

#include <iostream>

bool checkParametersSet(const json& json_object, const std::vector<std::string> params_names)
{
	for (const std::string& param : params_names)
	{
		if (json_object.count(param) == 0)
		{
			//std::cout << json_object[param] << std::endl;
			Log::Error("Parameter "+  param + " was not found.");
			return false;
		}
	}
	return true;
}

namespace Hyperion {

	Engine* Hyperion::EngineFoundry::assemble_engine_from_json(const std::string source)
	{
		/*!
		* \brief This method assembles a simulation engine from json source file.
		*/

		// Gathering json data structure
		std::ifstream input_filestream(source);
		if(!input_filestream.is_open())
			Log::Fatal("Json EngineFoudry : File not found["+source+"]");
		json json_source;
		input_filestream >> json_source;
		input_filestream.close();

		// Checking input file integrity
		if (!json_source["simulation"].is_object())
		{
			Log::Fatal("Simulation field not found in json source aborting.");
		}

		auto& json_sim = json_source["simulation"];

		if (!checkParametersSet(
			json_sim,
			{
				"watchdog_condition",
				"token",
				"grid",
				"boundaries",
				"analysis",
				"source",
				"geometry"
			}))
		{
			Log::Fatal("Critical parameter group missing in input file.");
		}

		//Log::Info("Building simulation engine with token: " + json_sim["token"]);
		Log::Info("Configuration loaded from [" + source + "]");

		Vec3<std::size_t> domain_size;
		if (json_sim["grid"].count("size") > 0 && json_sim["grid"]["size"].size() == 3)
		{
			domain_size.x = json_sim["grid"]["size"][0];
			domain_size.y = json_sim["grid"]["size"][1];
			domain_size.z = json_sim["grid"]["size"][2];
		}
		else
		{
			Log::Fatal("Grid size not correct.");
		}

		Engine* e = new Engine(domain_size, json_sim["token"]);

		e->setWatchdogCondition(json_sim["watchdog_condition"]);

		/*
			Loading layers
		*/
		
		for (auto& geometry : json_sim["geometry"])
		{
			if (geometry["type"].is_string()) {
				if (geometry["type"] == "layer")
				{
					auto mylayer = new Layer(geometry["depth"], geometry["epsilon"], geometry["position"]);
					e->getGeometryManager().appendLayer(mylayer);
				}
				else if (geometry["type"] == "sphere")
				{
					auto& pos = geometry["position"];
					if (pos.is_array() && pos.size() == 3)
					{
						Geometry* mysphere = new Sphere(geometry["radius"], pos[0], pos[1], pos[2], geometry["epsilon"]);
						e->getGeometryManager().appendGeometry(mysphere);
					}

				}
			}
		}
		
		/*
			Cuboid through wich the 1D simulation communicates with the 3D one.
		*/

		Cuboid TFSF;
		{
			auto& source = json_sim["source"];
			if (source.is_object())
			{
				if (checkParametersSet(source, { "box_start", "box_end" }))
				{
					if (
						   source["box_start"].is_array() && source["box_start"].size() == 3
						&& source["box_end"].is_array()   && source["box_end"].size() == 3
						)
					{
						TFSF.i0 = source["box_start"][0];
						TFSF.i1 = source["box_end"][0];
						TFSF.j0 = source["box_start"][1];
						TFSF.j1 = source["box_end"][1];
						TFSF.k0 = source["box_start"][2];
						TFSF.k1 = source["box_end"][2];
						e->getHuygensInterface().setCuboid(TFSF);
					}
					else
					{
						Log::Fatal("Failed to load tfsf box");
					}
				}
				else
				{
					Log::Fatal("Failed to load source data.");
				}

				TimeDomainSourceParams source_params;

				if (source["profile"].is_string())
				{
					if ((source["profile"] == "mexihat"))
						source_params.type = TimeDomainSourceParams::Profile::Mexihat;
					else if ((source["profile"] == "gaussian"))
						source_params.type = TimeDomainSourceParams::Profile::Gaussian;
					else
						Log::Fatal("Unsupported source type.");
				}
				else
				{
					Log::Fatal("Source profile unspecified.");
				}
				
				if (checkParametersSet(source, { "max_frequency", "wait", "power", "position_x" }))
				{
					source_params.frequency = source["max_frequency"];
					source_params.delta = source["wait"];
					source_params.power = source["power"];
					source_params.position = source["position_x"];
				}
				else
				{
					Log::Fatal("Missing source parameter.");
				}
				e->getAuxilaryDomain().init(source_params, e->get_size().x, e->getGeometryManager().getBackgroundVector());
			}
		}


	

		/*
			Poynting flux measurement planes.
		*/

		FlowPlaneProbe horiz_probe, late_probe;
		horiz_probe.offset = TFSF.i0 + 3;
		horiz_probe.position = { TFSF.j0 + 3, TFSF.k0+1};
		horiz_probe.size = { (TFSF.j1-2) - (TFSF.j0 + 3), (TFSF.k1-1) - (TFSF.k0+1) };

		late_probe.offset = TFSF.j0 + 3;
		late_probe.position = { TFSF.i0 + 3, TFSF.k0+1};
		late_probe.size = { (TFSF.i1 -2) - (TFSF.i0 + 3), (TFSF.k1-1) - (TFSF.k0+1) };

		/*
			Geometry manager
		*/

		json& boundary_data = json_sim["boundaries"];
		e->getGeometryManager().setBackgroundEpsilon(json_sim["grid"]["background_epsilon"]);
		e->getGeometryManager().setBackgroundType(BackgroundType::LAYERED);
		e->getGeometryManager().bakeGeometries(e->getEpsilon());

		/*
			PML and boundary conditions
		*/

		e->getPML().loadFromFile("fdtd/constants/cpml.txt");
		e->getPML().init(domain_size, boundary_data["cpml_width"], e->getEpsilon());
		e->init();

		/*
			AnalysisManager
		*/

		if (json_sim["analysis"].is_object()) {
			if (json_sim["analysis"]["snapshots"].is_array() && json_sim["analysis"]["snapshots"].size() > 0)
			{
				for (auto& snap : json_sim["analysis"]["snapshots"])
				{
					SnapshotAxis axis;
					SnapshotFieldType type;
					std::size_t level;
					std::vector<real> frequencies;
					Field<real>* field_ref = nullptr;
					if (snap["probefield"].is_string()) 
					{
						std::string temp = snap["probefield"];
						if (temp.size() == 2)
						{
							if (temp[0] == 'e') {
								type = SnapshotFieldType::E;
								switch (temp[1]) 
								{
								case 'x': case 'X':
									field_ref = &e->E.x;
									break;
								case 'y': case 'Y':
									field_ref = &e->E.y;
									break;
								case 'z': case 'Z':
									field_ref = &e->E.z;
									break;
								default:
									Log::Fatal("Unrecognized field component in snapshot.");
									break;
								}
							}
							else if (temp[0] == 'h') 
							{
								type = SnapshotFieldType::H;
								switch (temp[1]) {
								case 'x': case 'X':
									field_ref = &e->H.x;
									break;
								case 'y': case 'Y':
									field_ref = &e->H.y;
									break;
								case 'z': case 'Z':
									field_ref = &e->H.z;
									break;
								default:
									Log::Fatal("Unrecognized field component in snapshot.");
									break;
								}
							}
							else
								Log::Fatal("Unrecognized field type in snapshot.");		
						}

					}
					else
					{
						Log::Fatal("wrong probefield");
					}

					if (snap["plane"].is_string()) 
					{
						std::string temp = snap["plane"];
						if (temp.size() == 2)
						{
							if (temp == "xy")
								axis = SnapshotAxis::Z;
							else if (temp == "xz")
								axis = SnapshotAxis::Y;
							else if (temp == "yz")
								axis = SnapshotAxis::X;			
						}
					}
					else
					{
						Log::Fatal("wrong plane");
					}
					if (snap["level"].is_number_integer())
					{
						level = (std::size_t)snap["level"];
					}
					if (snap["frequencies"].is_array() && snap["frequencies"].size() > 0)
					{
						for (std::size_t i = 0; i < snap["frequencies"].size(); ++i)
						{
							if(snap["frequencies"][i].is_number_float())
								frequencies.push_back(snap["frequencies"][i]);
						}
					}
					else
					{
						Log::Error("Wrong frequencies.");
					}


					e->getAnalysManager().registerSnapshot(axis, type, level, frequencies, field_ref);
				}
			}
		}

		json& analysis_data = json_sim["analysis"];
		e->getAnalysManager().init(
			analysis_data["fluxes"]["fmin"], 
			analysis_data["fluxes"]["fmax"],
			analysis_data["fluxes"]["sampling"], 
			horiz_probe, 
			late_probe
		);
		
		return e;
	}
}