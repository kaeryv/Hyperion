#pragma once

#include "Geometry.h"
#include <math.h>

class Sphere : public Geometry
{
public:
	Sphere();
	Sphere(real radius, real x, real y, real z, real epsilon);
	BakeStatus bake(Field<real>& epsilon) override;
	~Sphere();
public:
	real Radius;
};

