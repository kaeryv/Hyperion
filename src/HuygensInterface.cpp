#include "./HuygensInterface.h"

/*
Extended source is based on and auxilary FDTD grid in order to provide source values.
- HuygensInterface : TFSF interface.
- vector<real> : Background epsilon.
- AuxilaryDomain : The aux. FDTD grid.
*/

void HuygensInterface::setCuboid(const Cuboid& injection_cuboid)
{
    m_InjectionCuboid = injection_cuboid;    
}

void HuygensInterface::consistencyH(VectorField & E, const AuxilaryYeeDomain& aux, const Field<real>& eps)
{

    Cuboid& TFSF = m_InjectionCuboid;
#pragma omp parallel for
    for (std::size_t j = TFSF.j0; j < TFSF.j1; j++) {
        for (std::size_t k = TFSF.k0; k < TFSF.k1; k++) {
            E.z(TFSF.i0, j + 1, k) -=
                fac_cfl*aux.getHy()[TFSF.i0 - 1]/eps(TFSF.i0 - 1,1,1); // ADDED -1 for TFSF epsion NOT VERIFIED
            
            E.z(TFSF.i1, j + 1, k) +=
                fac_cfl*aux.getHy()[TFSF.i1] / eps(TFSF.i1,1,1);
        }
    }

#ifndef Z_PERIODIC
#pragma omp parallel for
    for (std::size_t i = TFSF.i0; i < TFSF.i1; i++) {
        for (std::size_t j = TFSF.j0; j < TFSF.j1; j++) {
            E.x(i, j + 1, TFSF.k0) += fac_cfl * aux.getHy()[i] / eps(i, 1, 1);
            E.x(i, j + 1, TFSF.k1) -= fac_cfl * aux.getHy()[i] / eps(i, 1, 1);
        }
    }
#endif
}

void HuygensInterface::consistencyE(VectorField & H, const AuxilaryYeeDomain& aux)
{
    //Vec3<size_t> size = H.getSize();
    Cuboid& TFSF = m_InjectionCuboid;

#pragma omp parallel for
    for (std::size_t i = TFSF.i0; i <= TFSF.i1; i++) {
        for (std::size_t k = TFSF.k0; k < TFSF.k1; k++) {
            H.x(i, TFSF.j0, k) += fac_cfl*aux.getEz()[i];
            H.x(i, TFSF.j1, k) -= fac_cfl*aux.getEz()[i];
        }
    }
#pragma omp parallel for
    for (std::size_t j = TFSF.j0; j < TFSF.j1; j++) {
        for (std::size_t k = TFSF.k0; k < TFSF.k1; k++) {
            H.y(TFSF.i0 - 1, j + 1, k) -= fac_cfl*aux.getEz()[TFSF.i0];
            H.y(TFSF.i1, j + 1, k) += fac_cfl*aux.getEz()[TFSF.i1];
        }
    }
}


HuygensInterface::~HuygensInterface()
{
}
