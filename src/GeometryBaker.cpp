#include "./GeometryBaker.h"

#include "./Log.h"

GeometryBaker::GeometryBaker()
{
    m_BackgroundEpsilon = 1.0;
}

void GeometryBaker::appendGeometry(Geometry *g)
{
    m_Geometries.push_back(g);
}

void GeometryBaker::appendLayer(Layer *l)
{
    m_Layers.push_back(l);
}

void GeometryBaker::bakeGeometries(Field<real> &eps)
{

    Vec3<std::size_t> &size = eps.size;

    Log::Info("Baking geometries.");

    if (m_Type == BackgroundType::UNIFORM)
    {
        eps.fill(m_BackgroundEpsilon);
    }
    else if (m_Type == BackgroundType::LAYERED)
    {
        eps.fill(m_BackgroundEpsilon);
        for (Layer *l : m_Layers)
        {
            for (std::size_t i = l->getPosition(); i < l->getPosition() + l->getDepth(); i++)
            {
                for (std::size_t j = 0; j < size.y; j++)
                {
                    for (std::size_t k = 0; k < size.z; k++)
                    {
                        eps(i, j, k) = l->getEpsilon();
                    }
                }
            }
        }
    }
    for (Geometry *g : m_Geometries)
    {
        g->bake(eps);
    }
    m_Background.assign(size.x, 0.0);
    for (std::size_t i = 0; i < size.x; i++)
    {
        m_Background[i] = eps(i, 1, 1);
    }
}

std::vector<real> *GeometryBaker::getBackgroundVector()
{
    return &m_Background;
}

GeometryBaker::~GeometryBaker()
{
}
