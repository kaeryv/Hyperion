#include "VectorField.h"



VectorField::VectorField(Vec3<size_t> size, std::string name)
{
    m_Name = name;
	init(size, m_Name);
}

void VectorField::init(Vec3<size_t> size, std::string name)
{
    m_Name = name;
    x.init(size, m_Name + "->x");
	y.init(size, m_Name + "->y");
	z.init(size, m_Name + "->z");
}

void VectorField::zeros()
{
	fill(0.0);
}

void VectorField::fill(real value)
{
	fill(Vec3<real>{value, value, value});
}

void VectorField::fill(Vec3<real> values)
{
	x.fill(values.x);
	y.fill(values.y);
	z.fill(values.z);
}


void VectorField::ones()
{
	fill(1.0);
}

VectorField::~VectorField()
{
}
