#include "./Sphere.h"

Sphere::Sphere()
{
	m_Position = 0.0;
	Radius = 1.0;
}

Sphere::Sphere(real radius, real x, real y, real z, real epsilon) : Radius(radius)
{
	m_Position = {x, y, z};
	m_Epsilon = epsilon;
}

BakeStatus Sphere::bake(Field<real> &epsilon)
{
	Vec3<signed int> size = {(signed int)epsilon.size.x, (signed int)epsilon.size.y, (signed int)epsilon.size.z};
	Geometry *parent = this->getParent();
	Vec3<real> position = m_Position;
	while (parent != nullptr)
	{
		position += parent->getPosition();
		parent = parent->getParent();
	}

	// We iterate over circonscript cube to get faster processing.
	for (signed int i = floor(position.x - Radius); i <= ceil(position.x + Radius); i++)
	{
		for (signed int j = floor(position.y - Radius); j <= ceil(position.y + Radius); j++)
		{
			for (signed int k = floor(position.z - Radius); k <= ceil(position.z + Radius); k++)
			{
				real dist = pow(i - position.x, 2) + pow(j - position.y, 2) + pow(k - position.z, 2);

				if (dist < (Radius * Radius))
				{
					if (i >= 0 && j >= 0 && k >= 0 && i < size.x && j < size.y && k < size.z)
					{
						epsilon(i, j, k) += (m_Epsilon - epsilon(i, j, k));
					}
				}
				else if (dist <= (Radius * Radius))
					if (i > 0 && j > 0 && k > 0 && i < size.x && j < size.y && k < size.z)
						epsilon(i, j, k) += (m_Epsilon - epsilon(i, j, k)) * 0.5;
			}
		}
	}
	return BakeStatus::SUCCES;
}

Sphere::~Sphere()
{
}
