#include "./Log.h"

Log::Level Log::m_LogLevel = Log::LevelWarning;
bool Log::m_QueryOn = true;

void Log::Info(std::string s)
{
	if (m_LogLevel >= LevelInfo)
		std::cout << "[INFO] " << s << std::endl;
}

void Log::Warning(std::string s)
{
	if (m_LogLevel >= LevelWarning)
		std::cout << "[WARNING] " << s << std::endl;
}

void Log::Error(std::string s)
{
	if (m_LogLevel >= LevelError)
	{
		std::cout << "[ERROR] " << s << std::endl;
		//std::cin.get();
		exit(-1);
	}
}
void Log::Fatal(std::string s)
{
	if (m_LogLevel >= LevelError)
	{
		std::cout << "[FATAL] " << s << std::endl;
		//std::cin.get();
		exit(-2);
	}
}

void Log::Query(std::string s)
{
	if (m_LogLevel >= LevelWarning)
	{
		std::cout << "[QUERY] " << s << std::endl;
		if(m_QueryOn)
			std::cin.get();
	}

}
