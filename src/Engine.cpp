#include "./Engine.h"

// OpenMP
#include <omp.h>


static std::map<std::string, std::string> OutputFilenames =
{
	{ "reflexion",    "reflexion.csv" },
	{ "transmission", "transmission.csv" },
	{ "lateral",      "lateral.csv" },
	{ "watchdog",     "watchdog.csv" },
	{ "source",        "source.csv"}
};

namespace Hyperion {

    Engine::Engine(Vec3<size_t> size, const std::string& name) : YeeDomain(size), m_Name(name) {
        Log::Hbar();
        Log::Info("Launching FDTD engine.");
    }

    void Engine::init() {
        pml.bakeUpdateCoefs(deno);
    }


    void Engine::update(size_t T) {
		double t_start, t_end;

		t_start = omp_get_wtime();

        this->updateElectricField();

        pml.updateAndApplyE(E, H, epsilon, deno);

        hi.consistencyH(E, aux1D, epsilon);

        updateMagneticField();

        pml.updateAndApplyH(E, H, deno);
        hi.consistencyE(H, aux1D);
        am.update(E, H, T);
        am.updateSource(aux1D, T);
		am.updatePoyntingSnapshot(T);
        aux1D.update(T, epsilon);

		t_end = omp_get_wtime();

		if (!(T % 100)) {
			// Convergence condition
			real cond = getAnalysManager().updateWatchdog(T, E.z);
			if (cond<m_WatchdogCond && T > 1000) {
				request_close = true;
			}
			// Cout progress
			last_frametime = int(1000 * (t_end - t_start));
			Log::Info("Updated fields in " + std::to_string(last_frametime) + " ms. Watchdog: " + std::to_string(int(cond)) + " over " + std::to_string(m_WatchdogCond));


		}
    }


	IOError Engine::finalize()
	{
		std::ofstream f("io/" + m_Name +"/output.json");
		
		json json_output;
		
		json_output["simulation"] = {};
		json_output["simulation"]["analysis"]["horiz_surface"] = this->getAnalysManager().getHorizontalPlaneSurface();
		for (const auto& pair : OutputFilenames)
		{
			json_output["simulation"]["output"][pair.first] = pair.second;
		}

		f << json_output.dump();
		f.close();
		return IOError::NONE;
	}

	Engine::~Engine() {
    }
}