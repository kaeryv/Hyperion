#pragma once


#include "AuxilaryYeeDomain.h"
#include "VectorField.h"
#include "utils/CKinds.h"

struct Cuboid {
    size_t i0, i1, j0, j1, k0, k1;
};

class HuygensInterface
{
public:
    HuygensInterface(){}
    void setCuboid(const Cuboid& injection_cuboid);
    void consistencyH(VectorField& E, const AuxilaryYeeDomain&, const Field<real>& eps);
    void consistencyE(VectorField& H, const AuxilaryYeeDomain&);
    const Cuboid& get_cuboid() const {return m_InjectionCuboid;}
    ~HuygensInterface();
private:
    const real fac_cfl = 0.5;
    Cuboid m_InjectionCuboid;
};

