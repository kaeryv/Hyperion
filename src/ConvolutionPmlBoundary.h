#pragma once

#include <vector>
#include "utils/CKinds.h"
#include "Field.h"
#include "YeeDomain.h"
#include "PerfectMatchedLayer.h"


constexpr real fac_cfl = 0.5;
constexpr real DupA = 1.0, DupB = 1.0;
constexpr real CupA = 1.0, CupB = 1.0;

class ConvolutionPmlBoundary : public PerfectMatchedLayer
{
public:
	ConvolutionPmlBoundary();
	void init(const Vec3<std::size_t>& size, const std::size_t mWidth, const Field<real>& epsilon);
	void bakeUpdateCoefs(UpdateCoefficients& deno);
	void updateAndApplyH(const VectorField& E, VectorField& H, const UpdateCoefficients& deno);
    void updateAndApplyE(VectorField& E, const VectorField& H, const Field<real>& EPS, const UpdateCoefficients& deno);
	~ConvolutionPmlBoundary(){}
private:

    Vec3<std::size_t> n;

    bool m_IsInitialized;
	Field<real> psiExy1;
	Field<real> psiExy2;
	Field<real> psiExz1;
	Field<real> psiExz2;

	Field<real> psiEyx1;
	Field<real> psiEyx2;
	Field<real> psiEyz1;
	Field<real> psiEyz2;

	Field<real> psiEzx1;
	Field<real> psiEzx2;
	Field<real> psiEzy1;
	Field<real> psiEzy2;

	Field<real> psiHxy1;
	Field<real> psiHxy2;
	Field<real> psiHxz1;
	Field<real> psiHxz2;

	Field<real> psiHyx1;
	Field<real> psiHyx2;
	Field<real> psiHyz1;
	Field<real> psiHyz2;

	Field<real> psiHzx1;
	Field<real> psiHzx2;
	Field<real> psiHzy1;
	Field<real> psiHzy2;
};

