#pragma once

#include "utils/json.hpp"
#include "Engine.h"


bool checkAllParameters(const json& json_object, std::vector<std::string> params_names);


using json = nlohmann::json;

namespace Hyperion {

	class EngineFoundry
	{
	public:
		enum class Status {
			SUCCES, WARNING, ERROR
		};

		EngineFoundry() = delete;
		~EngineFoundry() = delete;

		static Engine* assemble_engine_from_json(const std::string source);

	};
}

