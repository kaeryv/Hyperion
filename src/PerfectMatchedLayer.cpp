#include "./PerfectMatchedLayer.h"

PerfectMatchedLayer::PerfectMatchedLayer()
{
}

void PerfectMatchedLayer::loadFromFile(const std::string filename)
{
    std::ifstream f(filename);
    if (!f.is_open())
        Log::Fatal("Cpml data not found.");
    std::string line, sbuffer;
    real rbuffer;
    while (getline(f, line))
    {
        std::stringstream ss(line);
        ss >> sbuffer >> rbuffer;
        m_Parameters[sbuffer] = rbuffer;
    }
    f.close();
}

void PerfectMatchedLayer::generate(std::size_t pml, std::size_t length, const Field<real> &epsilon)
{
    /*
    m_Width = pml;
    Imp0 = 1.0;
    epseff = 1.0;
    lnRtheo = -16;
    msk = 3.0;
    rapSmSo = 0.7;
    alphamax = 0.0;
    kappamax = 17.0;
    ma = 2.0;
    */
    m_Width = (std::size_t)m_Parameters["width"];
    Imp0 = (real)m_Parameters["Imp0"];         //1.0;
    epseff = (real)m_Parameters["epseff"];     //1.0;
    lnRtheo = (real)m_Parameters["lnRtheo"];   //-16;
    msk = (real)m_Parameters["msk"];           //3.0;
    rapSmSo = (real)m_Parameters["rapSmSo"];   //0.7;
    alphamax = (real)m_Parameters["alphamax"]; //0.0;
    kappamax = (real)m_Parameters["kappamax"]; //17.0;
    ma = (real)m_Parameters["ma"];             //2.0;

    Log::Hbar();
    Log::Info("Setting-up PML coefficients.");
    Log::Info("PML width : " + std::to_string(m_Width));

    // Optimal value for max conductivity following transmission lines theory
    real sigmaopt = -lnRtheo * (msk + 1) / (2.0 * Imp0 * (m_Width - 1) * sqrt(epseff));
    real sigmamax = rapSmSo * sigmaopt;

    // Conductivity and other profiles
    sigmaE = (real *)calloc(m_Width, sizeof(real));
    alphaE = (real *)calloc(m_Width, sizeof(real));
    kappaE = (real *)calloc(m_Width, sizeof(real));

    sigmaH = (real *)calloc(m_Width, sizeof(real));
    alphaH = (real *)calloc(m_Width, sizeof(real));
    kappaH = (real *)calloc(m_Width, sizeof(real));

    bE.init(1, length, pml, "bE");
    cE.init(1, length, pml, "cE");
    bH.init(1, length, pml, "bH");
    cH.init(1, length, pml, "cH");

#pragma omp parallel for
    for (std::size_t x = 0; x < length; x++)
    {
        for (std::size_t k = 0; k < m_Width; k++)
        {
            // Polynomial profiles
            sigmaE[k] = sigmamax /* / sqrt(epsilon(x, 0, 0))*/ * pow(((m_Width - k - 1.0) / (m_Width - 1.0)), msk);
            alphaE[k] = alphamax * pow(((k) / (m_Width - 1.0)), ma);
            kappaE[k] = 1.0 + (kappamax - 1.0) * pow(((m_Width - k - 1.0) / (m_Width - 1.0)), msk);

            // From wich we get the coefficients grading

            bE(0, x, k) = exp(-sigmaE[k] / kappaE[k] - alphaE[k]);
            if ((alphaE[k] == (0.0)) && (k == m_Width - 1))
            {
                cE(0, x, k) = 0.0;
            }
            else
            {

                cE(0, x, k) = sigmaE[k] * (bE(0, x, k) - 1.0) / (sigmaE[k] + kappaE[k] * alphaE[k]) / kappaE[k];
            }
        }
    }
        // Same for second set of coefficients, taking account of the <Yee shift>
#pragma omp parallel for
    for (std::size_t x = 0; x < length; x++)
    {
        for (std::size_t k = 0; k < pml; k++)
        {
            sigmaH[k] = sigmamax /*/ sqrt(epsilon(x, 0, 0))*/ * std::pow(((m_Width - k - 1.5) / (m_Width - 1.0)), msk);
            alphaH[k] = alphamax * std::pow(((k - 1.5) / (m_Width - 1.0)), ma);
            kappaH[k] = 1.0 + (kappamax - 1.0) * pow(((m_Width - k - 1.5) / (m_Width - 1.0)), msk);
            bH(0, x, k) = exp(-sigmaH[k] / kappaH[k] - alphaH[k]);
            cH(0, x, k) = sigmaH[k] * (bH(0, x, k) - 1.0) / (sigmaH[k] + kappaH[k] * alphaH[k]) / kappaH[k];
        }
    }
}

PerfectMatchedLayer::~PerfectMatchedLayer()
{
    delete[] sigmaE;
    delete[] alphaE;
    delete[] kappaE;

    delete[] sigmaH;
    delete[] alphaH;
    delete[] kappaH;
}
