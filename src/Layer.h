#pragma once

#include "utils/VectorUtils.h"
#include "utils/CKinds.h"

enum class Polarisation
{
	TM,
	TE
};
enum class LayerError
{
	SUCCES,
	WRONG_POLAR
};

class Layer
{
  public:
	Layer();
	Layer(std::size_t depth, real epsilon, std::size_t position);
	LayerError computeTMatrix(real omega, Polarisation polar);
	std::size_t getDepth() { return m_Depth; }
	std::size_t getPosition() { return m_Position; }
	real getEpsilon() { return m_Epsilon; }
	Vec2<complex> getAmplitude();
	~Layer();

  private:
	real m_Epsilon;
	std::size_t m_Depth;
	std::size_t m_Position;
};
