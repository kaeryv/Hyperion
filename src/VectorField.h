#pragma once

#include "utils/VectorUtils.h"
#include "utils/CKinds.h"

#include "Field.h"

class VectorField
{
public:
	VectorField(){}
	VectorField(Vec3<size_t> size, std::string name);
	void init(Vec3<size_t> size, std::string name);
	void zeros();
	void fill(real value);
	void fill(Vec3<real> values);
	void ones();
	const Vec3<size_t>& getSize() const noexcept { return x.size(); }
	~VectorField();
public:
	Field<real> x, y, z;
    std::string m_Name;
};

