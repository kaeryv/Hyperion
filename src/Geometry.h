#pragma once

#include "utils/CKinds.h"
#include "Field.h"

enum class BakeStatus
{
	SUCCES,
	OUT_OF_DOMAIN,
	INVALID_GEOMETRY
};

class Geometry
{
  public:
	Geometry();
	~Geometry();
	virtual BakeStatus bake(Field<real> &modellingSpace) = 0;
	Geometry *getParent() { return m_Parent; }
	void setEpsilon(real eps) { m_Epsilon = eps; }
	Vec3<real> &getPosition() { return m_Position; }
	bool isInfinite() { return m_Infinite; }

  public:
	Vec3<real> m_Position;

  protected:
	real m_Epsilon;
	Geometry *m_Parent;
	bool m_Infinite;
};
