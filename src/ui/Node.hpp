#pragma once

#include <SFML/Graphics.hpp>


class Node : public sf::Transformable
{
public:
	Node();
	Node(Node* parent);
	const Node* getParent();
	void setParent(Node* parent);
	~Node();
private:
	Node * m_Parent;
};

