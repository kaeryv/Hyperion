#include "FieldTexture1D.h"



FieldTexture1D::FieldTexture1D(const Hyperion::Engine& simulation)
{
	// RGBA uint8 pixels buffer allocation.
	{
		// Number of pixels
		unsigned pixels_number = simulation.getAuxilaryDomain().getEz().size().x * 100;
		// Memory buffer length
		unsigned buffer_length = 4 * pixels_number;
		m_Pixels = new sf::Uint8[buffer_length];
		if (!m_Pixels)
			Log::Fatal("Unable to allocate memory for texture buffer in FieldTexture.");
		// Ensure correct initialization.
		std::fill_n(m_Pixels, pixels_number * 4, 0);
	}
	this->create(simulation.getAuxilaryDomain().getEz().size().x, m_AmplitudeSampling);
	m_Domain = &simulation.getAuxilaryDomain();
}

void FieldTexture1D::update()
{
	this->clear(sf::Color::Transparent);
	for (std::size_t k = 0; k < m_Domain->getEz().size().x; k+=3)
	{
		
		sf::CircleShape dot;
		real amplitude = m_Domain->getEz()[k];
		dot.setFillColor(sf::Color::Red);
		dot.setRadius(1);
		dot.setScale(1, 1);
		dot.setPosition(k, m_AmplitudeSampling/2 +  int(round(amplitude*100)));
		
		this->draw(dot);

	}

}

FieldTexture1D::~FieldTexture1D()
{
}
