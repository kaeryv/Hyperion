#include "Node.hpp"




Node::Node()
{
}

Node::Node(Node * parent)
{
	m_Parent = parent;
}

const Node * Node::getParent()
{
	return m_Parent;
}

void Node::setParent(Node * parent)
{
	m_Parent = parent;
}

Node::~Node()
{
}
