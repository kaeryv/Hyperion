#include "./FieldTexture.h"

real FieldTexture::ContrastFactor = 1.0;

FieldTexture::FieldTexture(const Field<real>& field, const DisplayAxis& da)
{
	// We keep the display axis.
	m_Axis = da;
	// We set the init slice to center.
	m_Slice = .5;
	// From the axis, we define the size of the 2D texture used.
	// Also, we default the 
	switch (m_Axis)
	{
	case DisplayAxis::X:
		m_DisplaySize.x = field.size().y;
		m_DisplaySize.y = field.size().z;
		break;
	case DisplayAxis::Y:
		m_DisplaySize.x = field.size().x;
		m_DisplaySize.y = field.size().z;
		break;
	case DisplayAxis::Z:
		m_DisplaySize.x = field.size().x;
		m_DisplaySize.y = field.size().y;
		break;
	default:
		Log::Fatal("Invalid DisplayAxis specified to FieldTexture.");
	}

	// We ask sfml parent to create it's texture object.
	if (!create(m_DisplaySize.x, m_DisplaySize.y))
	{
		Log::Fatal("Unable to create texture in FieldTexture.");
	}

	// RGBA uint8 pixels buffer allocation.
	{
		// Number of pixels
		m_Length = m_DisplaySize.x * m_DisplaySize.y;
		// Memory buffer length
		unsigned buffer_length = 4 * m_Length;
		m_Pixels = new sf::Uint8[buffer_length];
		if (!m_Pixels)
			Log::Fatal("Unable to allocate memory for texture buffer in FieldTexture.");
		// Ensure correct initialization.
		std::fill_n(m_Pixels, m_Length * 4, 0);
	}
}

void FieldTexture::updateFromField(const Field<real> & field)
{
    real value;
	for (size_t i = 0; i < m_DisplaySize.x; i++)
	{
		for (size_t j = 0; j < m_DisplaySize.y; j++)
		{
            if (m_Axis == DisplayAxis::X)
                value = field((size_t)floor(field.size().x*m_Slice), i, j);
            else if (m_Axis == DisplayAxis::Y)
                value = field(i, (size_t)floor(field.size().y*m_Slice), j);
            else if (m_Axis == DisplayAxis::Z)
                value = field(i, j, (size_t)floor(field.size().z*m_Slice));
            else
                value = 0.;

			// Accounting for contrast control
            value *= ContrastFactor;
            
			// Changing color following the sign.
            if (value >= 0) 
            {
                m_Pixels[4 * (j*m_DisplaySize.x + i) + 2] = (sf::Uint8) 0;
                m_Pixels[4 * (j*m_DisplaySize.x + i) + 0] = (sf::Uint8) 200;
            }
            else 
            {
                m_Pixels[4 * (j*m_DisplaySize.x + i) + 0] = (sf::Uint8) 0;
                m_Pixels[4 * (j*m_DisplaySize.x + i) + 2] = (sf::Uint8) 200;
            }
			// Alpha channel set regarding the magnitude.
			m_Pixels[4 * (j*m_DisplaySize.x + i) + 3] = (sf::Uint8) floor(0.5*(std::abs(value)) * 255);
		}
	}
    update(m_Pixels);
}

void FieldTexture::updateFromEpsilon(const Field<real> & field)
{
    real value = 0.0;

    for (size_t i = 0; i < m_DisplaySize.x; i++)
        for (size_t j = 0; j < m_DisplaySize.y; j++)
        {
            if (m_Axis == DisplayAxis::X)
                value = field((size_t)floor(field.size().x*m_Slice), i, j);
            else if (m_Axis == DisplayAxis::Y)
                value = field(i, (size_t)floor(field.size().y*m_Slice), j);
            else if (m_Axis == DisplayAxis::Z)
                value = field(i, j, (size_t)floor(field.size().z*m_Slice));
            else
                value = 0.0;

			value = value - 1;
			value = value / 6.5;
			m_Pixels[4 * (j*m_DisplaySize.x + i) + 3] = (sf::Uint8) floor(value * 255);
        }
    update(m_Pixels);
}


FieldTexture::~FieldTexture()
{
	// Free pixels buffer.
    delete[] m_Pixels;
}

void FieldTexture::fillTexture(sf::Color filling_color)
{
	for (std::size_t i = 0; i < m_Length; i++) {
		m_Pixels[4 * i + 0] = filling_color.r;
		m_Pixels[4 * i + 1] = filling_color.g;
		m_Pixels[4 * i + 2] = filling_color.b;
		m_Pixels[4 * i + 3] = filling_color.a;
	}
}
