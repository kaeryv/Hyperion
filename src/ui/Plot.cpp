#include "Plot.h"


namespace Visual
{
	Plot::Plot()
	{

	}

	void Visual::Plot::append(std::vector<real> x, std::vector<real> y)
	{
		m_Lines.push_back({ x,y });

		if (m_Axis.state == Axis::State::AUTO) {
			// We adapt the axes if needed
			m_Axis.xmin = std::min(std::min_element(x.begin(), x.end())[0], m_Axis.xmin);
			m_Axis.xmax = std::max(std::max_element(x.begin(), x.end())[0], m_Axis.xmax);
			m_Axis.ymin = std::min(std::min_element(y.begin(), y.end())[0], m_Axis.ymin);
			m_Axis.ymax = std::max(std::max_element(y.begin(), y.end())[0], m_Axis.ymax);
		}
	}

	void Plot::draw(sf::RenderWindow & window)
	{
		m_Canvas.create(110, 110);
		m_Canvas.clear(sf::Color::Transparent);

		for (auto& line : m_Lines)
		{
			for (std::size_t k = 0; k < line.x.size(); ++k)
			{
				printf("%f", m_Axis.xmax);
				sf::CircleShape dot;
				
				real amplitude = line.y[k];
				dot.setFillColor(sf::Color::Red);
				dot.setRadius(5);
				dot.setScale(1, 1);

				real lx = m_Axis.xmax - m_Axis.xmin;
				real ly = m_Axis.ymax - m_Axis.ymin;

				dot.setPosition(100 * (line.x[k]/lx), 100*(amplitude/ly));

				m_Canvas.draw(dot);

			}
		}
		sf::Sprite temp(m_Canvas.getTexture());
		temp.setPosition(temp.getPosition()+this->getPosition());
		window.draw(temp);
	}


	Plot::~Plot()
	{

	}
}