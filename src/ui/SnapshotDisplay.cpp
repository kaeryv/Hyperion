#include "./SnapshotDisplay.h"



SnapshotDisplay::SnapshotDisplay(
	const Field<real>& field, 
	const Field<real>& epsilon, 
	const DisplayAxis& da) :

	m_EpsilonTexture(epsilon, da),
	m_FieldTexture(field, da)
{
	// Das beautifull yellow dielectric medium from Nature publications
	m_EpsilonTexture.fillTexture(sf::Color{244,206,66});

	// We need sfml sprites for rendering ...
    m_EpsilonSprite.setTexture(m_EpsilonTexture);
    m_FieldSprite.setTexture(m_FieldTexture);


	m_EpsilonTexture.setSlice(m_DepthViewFactor);
	m_EpsilonTexture.updateFromEpsilon(epsilon);

	background.setSize(sf::Vector2f{ (float)m_FieldTexture.getSize().x, (float)m_FieldTexture.getSize().y });
	background.setFillColor(sf::Color::White);
	
}

void SnapshotDisplay::draw(const Field<real>& field, const Field<real>& epsilon, sf::RenderWindow& win)
{
	// We need to recompute field texture each frame ...
    m_FieldTexture.setSlice(m_DepthViewFactor);
    m_FieldTexture.updateFromField(field);
	m_EpsilonTexture.setSlice(m_DepthViewFactor);
	m_EpsilonTexture.updateFromEpsilon(epsilon);
	
	// Ensure the background fits
	background.setPosition(m_FieldSprite.getPosition());
	background.setScale(m_FieldSprite.getScale());

	// Then draw it all in order
    win.draw(background);
    win.draw(m_EpsilonSprite);
    win.draw(m_FieldSprite);
}

/*
	We override setPostion to account for possible parents.
*/
void SnapshotDisplay::setPosition(sf::Vector2f pos)
{
	sf::Vector2f new_position = pos;
	if (this->getParent() != nullptr)
	{
		new_position += this->getParent()->getPosition();
	}
	
	Node::setPosition(new_position);
	m_EpsilonSprite.setPosition(new_position);
	m_FieldSprite.setPosition(new_position);
}

void SnapshotDisplay::setScale(Vec2<float> scale)
{
    m_EpsilonSprite.setScale(scale.x, scale.y);
    m_FieldSprite.setScale(scale.x, scale.y);
}


SnapshotDisplay::~SnapshotDisplay()
{
}
