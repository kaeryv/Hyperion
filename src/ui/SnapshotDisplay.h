#pragma once

#include "FieldTexture.h"
#include "Node.hpp"

class SnapshotDisplay : public Node
{
public:
    SnapshotDisplay(const Field<real>& field, const Field<real>& epsilon, const DisplayAxis& da);
    void draw(const Field<real>& field, const Field<real>& epsilon, sf::RenderWindow& win);
    void setPosition(sf::Vector2f pos);
    void setScale(Vec2<float> scale);
    void setDepthView(const float factor){m_DepthViewFactor = factor;}
    ~SnapshotDisplay();
private:
    sf::Sprite m_EpsilonSprite, m_FieldSprite;
	// Background of the snapshot
	sf::RectangleShape background;
    FieldTexture m_EpsilonTexture, m_FieldTexture;
    float m_DepthViewFactor = 0.5;
};

