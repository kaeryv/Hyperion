#include "./UserInterface.h"

#include "../Log.h"

#include "../utils/json.hpp"
#include "SnapshotDisplay.h"


#include "Plot.h"
#include <fstream>

using json = nlohmann::json;

float scale_factor = 1.0f;

static std::map<std::string, sf::Text::Style> sf_style_map = 
{ 
	{
		"bold", sf::Text::Bold 
	},
	{ 
		"italic", sf::Text::Italic 
	},
	{
		"regular", sf::Text::Regular
	},
	{
		"underlined", sf::Text::Underlined
	},
	{
		"striked", sf::Text::StrikeThrough
	} 
};

UserInterface::UserInterface(Hyperion::Engine& sim)
{
	json json_scheme;
	std::ifstream input_filestream("ui/schemes/default.json");
	input_filestream >> json_scheme;

	if (!json_scheme["interface"].is_object())
	{
		Log::Fatal("Critical failure while loading interface scheme, no interface.");
	}

	auto& json_interface = json_scheme["interface"];

	for (auto& interface_component : json_interface["components"])
	{
		if (interface_component["type"] == "label") {
			auto* dt = new Label("String component", m_Font, 12, false);
			
			if (interface_component["string"].is_string())
			{
				std::string value = interface_component["string"];
				dt->setString(value);
			}

			if (interface_component["color"].is_array() && interface_component["color"].size() == 3)
			{
				auto& color = interface_component["color"];
				dt->setFillColor(sf::Color(color[0], color[1], color[2]));
			}

			if (interface_component["position"].is_array() && interface_component["position"].size() == 2)
			{
				auto& position = interface_component["position"];
				dt->setPosition(position[0], position[1]);
			}

			if (interface_component["style"].is_string())
			{
				std::string value = interface_component["style"];
				if (sf_style_map[value])
					dt->setStyle(sf_style_map[value]);
			}
			if (interface_component["tokens"].is_array() && interface_component["tokens"].size() > 0)
			{
				dt->setDynamic(true);
				dt->setQualifiedText(dt->getString());
				for (auto& data : interface_component["tokens"])
				{
					if (data.is_string()) {
						dt->add_data_reference(data);
					}
				}
			}
			m_text_sprites.push_back(dt);
		}
	}


	aspect_ratio = { 1.0,1.0 };
    if (!m_Font.loadFromFile("ui/fonts/monaco.ttf")) {
        Log::Fatal("SFML encountered error while loading font.");
    }

    m_CurrentDisplayAxis = DisplayAxis::X;

    m_ShapeXAxis= sf::RectangleShape(sf::Vector2f(50, 3));
    m_ShapeXAxis.setFillColor(sf::Color::Blue);
    m_ShapeXAxis.setPosition(5,5);
    m_ShapeYAxis= sf::RectangleShape(sf::Vector2f(50, 3));
    m_ShapeYAxis.setPosition(5, 5);
    m_ShapeYAxis.setFillColor(sf::Color::Red);
    m_ShapeYAxis.rotate(90);

	initDisplay({ json_interface["window-size"][0],json_interface["window-size"][1]}, sim);

}

void UserInterface::setGridDimText(size_t x, size_t y, size_t z)
{
	m_display_data["fdtd.grid.lx"] = std::to_string(x);
	m_display_data["fdtd.grid.ly"] = std::to_string(y);
	m_display_data["fdtd.grid.lz"] = std::to_string(z);
}

void UserInterface::setFrameTimeText(real r, int T, float slice)
{
	m_display_data["system.frametime"] = std::to_string(int(r));
	m_display_data["system.timestep"] = std::to_string(int(T));
	m_display_data["system.slice"] = std::to_string(slice);
}

void UserInterface::draw(sf::RenderWindow& window)
{
    if (m_CurrentDisplayAxis == DisplayAxis::X) {
        m_ShapeXAxis.setFillColor(sf::Color::Blue);
        m_ShapeYAxis.setFillColor(sf::Color::Green);
    }
    else if (m_CurrentDisplayAxis == DisplayAxis::Y) {
        m_ShapeXAxis.setFillColor(sf::Color::Red);
        m_ShapeYAxis.setFillColor(sf::Color::Green);
    }
    else {
        m_ShapeXAxis.setFillColor(sf::Color::Red);
        m_ShapeYAxis.setFillColor(sf::Color::Blue);
    }
 
    window.draw(m_ShapeXAxis);
    window.draw(m_ShapeYAxis);

	ft1D->update();
	sf::Sprite temp;
	
	temp.setPosition(0, 400);
	temp.setTexture(ft1D->getTexture());
	temp.setScale(3.f, 1.f);
	window.draw(temp);


	//std::vector<real> x1 = { 1.,2.,3. };
	//std::vector<real> y1 = { 1.,2.,1. };
	//Visual::Plot plot;
	//plot.append(x1, y1);
	//plot.setPosition(100,100);
	//plot.draw(window);


	// Dynamic ui display
	for (std::size_t i = 0; i < m_text_sprites.size(); i++)
		m_text_sprites[i]->update_dynamic_text(m_display_data);
	for (const auto* thistext : m_text_sprites) {
		window.draw(*thistext);
	}
}

void UserInterface::drawHuygensInterf(const Cuboid& hi, sf::RenderWindow& win)
{
    if (m_CurrentDisplayAxis == DisplayAxis::X) {
        sf::RectangleShape tfsf = sf::RectangleShape({ (float)(hi.j1 - hi.j0),(float)(hi.k1 - hi.k0) });
		tfsf.setPosition(scale_factor*hi.j0, scale_factor*hi.k0);
        tfsf.scale(scale_factor, scale_factor);
        tfsf.setFillColor(sf::Color(0, 0, 0, 0));
        tfsf.setOutlineThickness(1.0);
        tfsf.setOutlineColor(sf::Color(0, 0, 0, 255));
		tfsf.setPosition(tfsf.getPosition() + m_FieldDisplayPane->getPosition());

        win.draw(tfsf);
    }else if (m_CurrentDisplayAxis == DisplayAxis::Y) {
        sf::RectangleShape tfsf = sf::RectangleShape({ (float)(hi.i1 - hi.i0), (float)(hi.k1 - hi.k0) });
        tfsf.setPosition(scale_factor*hi.i0, scale_factor*hi.k0);
        tfsf.scale(scale_factor, scale_factor);
        tfsf.setFillColor(sf::Color(0, 0, 0, 0));
        tfsf.setOutlineThickness(1.0);
        tfsf.setOutlineColor(sf::Color(0, 0, 0, 255));
		tfsf.setPosition(tfsf.getPosition() + m_FieldDisplayPane->getPosition());

        win.draw(tfsf);
    }
    else if (m_CurrentDisplayAxis == DisplayAxis::Z) {
        sf::RectangleShape tfsf = sf::RectangleShape({ (float)(hi.i1 - hi.i0), (float)(hi.j1 - hi.j0) });
		tfsf.setPosition(scale_factor*(hi.i0), scale_factor*(hi.j0+1));
        tfsf.scale(scale_factor, scale_factor);

        tfsf.setFillColor(sf::Color(0, 0, 0, 0));
        tfsf.setOutlineThickness(1.0);
        tfsf.setOutlineColor(sf::Color(0, 0, 0, 255));
		tfsf.setPosition(tfsf.getPosition() + m_FieldDisplayPane->getPosition());

        win.draw(tfsf);
    }
    
}

void UserInterface::setCurrentAxis(DisplayAxis da)
{
    m_CurrentDisplayAxis = da;
}

void UserInterface::update(const size_t T, const Hyperion::Engine& simulation, const size_t last_frame_time) {
	/*
	  Event Handling.
	*/
	sf::Event event;
	while (window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed) 
		{
			window->close();
			std::cin.get();
		}
		else if (event.type == sf::Event::KeyPressed) 
		{
			switch (event.key.code)
			{
			case sf::Keyboard::Add:
				FieldTexture::ContrastFactor += 0.1;
				break;
			case sf::Keyboard::Subtract:
				FieldTexture::ContrastFactor -= 0.1;
				break;
			case sf::Keyboard::Multiply:
				FieldTexture::ContrastFactor *= 10.0;
				break;
			case sf::Keyboard::Divide:
				FieldTexture::ContrastFactor /= 10.0;
				break;
			case sf::Keyboard::Up:
				m_CurrentDepthView += 0.005;
				if (m_CurrentDepthView > 0.99)
					m_CurrentDepthView = 0.99;
				snapshot1->setDepthView(m_CurrentDepthView);
				snapshot2->setDepthView(m_CurrentDepthView);
				snapshot3->setDepthView(m_CurrentDepthView);
				break;
			case sf::Keyboard::Down:
				m_CurrentDepthView -= 0.005;
				if (m_CurrentDepthView < 0.0)
					m_CurrentDepthView = 0.0;
				snapshot1->setDepthView(m_CurrentDepthView);
				snapshot2->setDepthView(m_CurrentDepthView);
				snapshot3->setDepthView(m_CurrentDepthView);
				break;
			case sf::Keyboard::C:
				m_CurrentDepthView = 0.5;
				snapshot1->setDepthView(m_CurrentDepthView);
				snapshot2->setDepthView(m_CurrentDepthView);
				snapshot3->setDepthView(m_CurrentDepthView);
				break;
			case sf::Keyboard::X:
				setCurrentAxis(DisplayAxis::X);
				break;
			case sf::Keyboard::Y:
				setCurrentAxis(DisplayAxis::Y);
				break;
			case sf::Keyboard::Z:
				setCurrentAxis(DisplayAxis::Z);
				break;
			default:
				break;
			}
		}
	}
	window->clear(sf::Color(50, 52, 55));

	float size_z = simulation.get_size().z;
	float display_scale = scale_factor;
	int repeat_z = 6;

	if (getCurrentAxis() == DisplayAxis::X) 
	{
		for (int i = 0; i <= repeat_z; i++)
		{
			snapshot1->setPosition({ 0, size_z * display_scale * i });
			snapshot1->draw(simulation.E.z, simulation.getEpsilon(), *window);
		}
	}
	else if (getCurrentAxis() == DisplayAxis::Y) 
	{
		for (int i = 0; i < repeat_z; i++)
		{
			snapshot2->setPosition({ 0, size_z * display_scale * i });
			snapshot2->draw(simulation.E.z, simulation.getEpsilon(), *window);
		}
	}
	else if (getCurrentAxis() == DisplayAxis::Z) 
	{
		snapshot3->setPosition({ 0, 0 });
		snapshot3->draw(simulation.E.z, simulation.getEpsilon(), *window);
		
	}


    setFrameTimeText(last_frame_time, T, m_CurrentDepthView);
    drawHuygensInterf(simulation.getHuygensInterface().get_cuboid(), *window);
    draw(*window);

    window->display();
}

void UserInterface::initDisplay(Vec2<unsigned> resolution, const Hyperion::Engine& simulation) {

    window = new sf::RenderWindow(
            sf::VideoMode(resolution.x, resolution.y),
			m_display_data["window.title"],
            sf::Style::Titlebar | sf::Style::Close);

	m_RootPane = new Node(nullptr);
	m_FieldDisplayPane = new Node(m_RootPane);
	m_FieldDisplayPane->setPosition(0, 80);
    // Epsilon field display.
    snapshot2 = new SnapshotDisplay(simulation.E.z, simulation.getEpsilon(), DisplayAxis::Y);
    snapshot1 = new SnapshotDisplay(simulation.E.z, simulation.getEpsilon(), DisplayAxis::X);
    snapshot3 = new SnapshotDisplay(simulation.E.z, simulation.getEpsilon(), DisplayAxis::Z);

	snapshot1->setParent(m_FieldDisplayPane);
	snapshot2->setParent(m_FieldDisplayPane);
	snapshot3->setParent(m_FieldDisplayPane);

    snapshot1->setScale({ scale_factor,scale_factor });
    snapshot2->setScale({ scale_factor,scale_factor });
    snapshot3->setScale({ scale_factor ,scale_factor });

	ft1D = new FieldTexture1D(simulation);

    setGridDimText(simulation.get_size().x, simulation.get_size().y, simulation.get_size().z);
}


