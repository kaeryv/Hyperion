#pragma once

#include <SFML/Graphics.hpp>

#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>

#include "../utils/CKinds.h"
#include "../HuygensInterface.h"
#include "../Engine.h"

#include "SnapshotDisplay.h"
#include "FieldTexture1D.h"



static const std::vector<std::string> explode(const std::string& s, const char& c)
{
	std::string buff{ "" };
	std::vector<std::string> v;

	for (auto n : s)
	{
		if (n != c) buff += n; else
			if (n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	v.push_back(buff);

	return v;
}

class Label : public sf::Text
{
public:
	Label(const std::string& text, const sf::Font& font, unsigned pt, bool is_dynamic) : 
		sf::Text(text, font, pt) 
	{
		if (is_dynamic)
		{
			m_string_buffer = explode(text, '$');
		}
		else 
		{
			m_string_buffer = explode(text, '$');
		}
		
	}
	void setQualifiedText(std::string str)
	{
		// Setting text means new qualified text
		m_string_buffer.clear();
		std::vector<std::string> temp = explode(str, '$');
		m_string_buffer.insert(m_string_buffer.end(), temp.begin(), temp.end());
	}
	virtual ~Label(){

	}
	void update_dynamic_text(const std::map<std::string, std::string>& data) {
		if (m_is_dynamic)
		{
			std::string text;

			for (unsigned i = 0; i < m_string_buffer.size(); i++)
			{
				text += m_string_buffer[i];
				if (m_data_references.size() > i)
					text += data.at(m_data_references[i]);
			}
			setString(text);
		}
	}
	void setDynamic(bool b)
	{
		m_is_dynamic = b;
	}
	void add_data_reference(const std::string& ref)
	{
		m_data_references.push_back(ref);
	}
private:
	std::vector<std::string> m_data_references;
	std::vector<std::string> m_string_buffer;
	bool m_is_dynamic = false;
};

class UserInterface
{
public:
    UserInterface(Hyperion::Engine& sim);
    void setGridDimText(size_t x, size_t y, size_t z);
    void setFrameTimeText(real r, int T, float slice);
    void draw(sf::RenderWindow& window);
    void drawHuygensInterf(const Cuboid& hi, sf::RenderWindow& win);
    void setCurrentAxis(DisplayAxis da);
    void update(const size_t T, const Hyperion::Engine& simulation, const size_t last_frame_time);
    bool resquest_close(){return !window->isOpen();}
    void initDisplay(Vec2<unsigned> resolution, const Hyperion::Engine& simulation);
    DisplayAxis getCurrentAxis() { return m_CurrentDisplayAxis; }
    ~UserInterface(){}
    Vec2<float> aspect_ratio;
private:
    sf::Font m_Font;
	std::vector<Label*> m_text_sprites;

    sf::RectangleShape m_ShapeXAxis;
    sf::RectangleShape m_ShapeYAxis;
    DisplayAxis m_CurrentDisplayAxis;

    sf::RenderWindow *window;
    
	SnapshotDisplay* snapshot1;
    SnapshotDisplay* snapshot2;
    SnapshotDisplay* snapshot3;

	FieldTexture1D* ft1D;

	Node* m_FieldDisplayPane;
	Node* m_RootPane;


    float m_CurrentDepthView = 0.5f;

	std::map<std::string, std::string> m_display_data;
};



