#pragma once

#include <SFML/Graphics.hpp>

#include "../utils/CKinds.h"
#include "../Field.h"
#include "../Log.h"

enum class DisplayAxis {
	X, Y, Z
};

class FieldTexture : public sf::Texture
{
public:
	/*
		Allocates space for the texture buffer
		4 * length * uint16
		Also initialize sfml texture and sprite.
	*/
	FieldTexture(const Field<real>& field, const DisplayAxis& da);

	/*
		Two methods for updating, due to two 'colormaps'
		Epsilon is all-in yellow/white (Need to find something better in the future)
		Field is represented by a gradient from blue to red
	*/
	void updateFromField(const Field<real>& field);
    void updateFromEpsilon(const Field<real>& field);
	
	/*
		Sometimes, it is necessary to clear the canvas.
	*/
	void fillTexture(sf::Color filling_color);

	~FieldTexture();

	/*
		Getter and setter nasty game.
	*/
	const Vec2<unsigned> getSize() const { return m_DisplaySize; }
	const float getSlice() const { return m_Slice; }
	void setSlice(const float slice) { m_Slice = slice; }
public:
    static real ContrastFactor;
private:
	// Pixels buffer
	sf::Uint8 *m_Pixels;
	// Texture dimensions
    Vec2<unsigned> m_DisplaySize;
	// Number of pixels
	unsigned m_Length;
	// Currently rendered slice.
	float m_Slice;
	// Axis displayed
	DisplayAxis m_Axis;
};

