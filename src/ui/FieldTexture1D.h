#pragma once

#include <SFML/Graphics.hpp>

#include "../Engine.h"

class FieldTexture1D : public sf::RenderTexture
{
public:
	FieldTexture1D(const Hyperion::Engine& sim);
	void update();
	~FieldTexture1D();
private:
	const AuxilaryYeeDomain * m_Domain;
	// SIze of the texture
	Vec2<int> m_Size;
	// Pixels buffer
	sf::Uint8 *m_Pixels;
	unsigned m_AmplitudeSampling = 300;
	unsigned m_PropagationSampling = 0;
};

