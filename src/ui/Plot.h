#pragma once

#include <SFML/Graphics.hpp>
#include "Node.hpp"
#include "FieldTexture1D.h"

/*
	This class provides a 
*/

namespace Visual {
	
	struct Line {
		std::vector<real> x,y;
	};
	struct Axis {
		enum class State {
			AUTO, FIXED, OFF
		};
		real xmin, xmax, ymin, ymax;
		State state = State::AUTO;
	};
	class Plot : public Node
	{
	public:
		Plot();
		void append(std::vector<real> x, std::vector<real> y);
		void draw(sf::RenderWindow& window);
		~Plot();

	private:
		sf::Sprite background;

		// Chart X, Y pairs
		std::vector<Line> m_Lines;
		sf::RenderTexture m_Canvas;

		Axis m_Axis;
	};
}
